;;; klere-fos--battle-maps.el --- Klere Fòs: Battle Maps                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Klere-F-s
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0
;; Keywords: game, rpg

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(require 'klere-fos--matrices)
(require 'klere-fos--maps)
;; toggle-truncate-lines
(require 'simple)

(defun klere-fos-battle-map-up ()
  ""
  (interactive)

  (read-only-mode 0)
  (setq klere-fos----battle-position (klere-fos--maps-move-cursor-position-up
                                       klere-fos----battle-matrix
                                       klere-fos----battle-position))
  (read-only-mode 1))

(defun klere-fos-battle-map-down ()
  ""
  (interactive)

  (read-only-mode 0)
  (setq klere-fos----battle-position (klere-fos--maps-move-cursor-position-down
                                       klere-fos----battle-matrix
                                       klere-fos----battle-position))
  (read-only-mode 1))

(defun klere-fos-battle-map-left ()
  ""
  (interactive)

  (read-only-mode 0)
  (setq klere-fos----battle-position (klere-fos--maps-move-cursor-position-left
                                       klere-fos----battle-matrix
                                       klere-fos----battle-position))
  (read-only-mode 1))

(defun klere-fos-battle-map-right ()
  ""
  (interactive)

  (read-only-mode 0)
  (setq klere-fos----battle-position (klere-fos--maps-move-cursor-position-right
                                       klere-fos----battle-matrix
                                       klere-fos----battle-position))
  (read-only-mode 1))

(defvar klere-fos--battle-mode-map (let ((mode-map (make-sparse-keymap)))
                                     (define-key mode-map (kbd "<up>")    #'klere-fos-battle-map-up)
                                     (define-key mode-map (kbd "p")       #'klere-fos-battle-map-up)
                                     (define-key mode-map (kbd "C-p")     #'klere-fos-battle-map-up)

                                     (define-key mode-map (kbd "<down>")  #'klere-fos-battle-map-down)
                                     (define-key mode-map (kbd "n")       #'klere-fos-battle-map-down)
                                     (define-key mode-map (kbd "C-n")     #'klere-fos-battle-map-down)

                                     (define-key mode-map (kbd "<left>")  #'klere-fos-battle-map-left)
                                     (define-key mode-map (kbd "b")       #'klere-fos-battle-map-left)
                                     (define-key mode-map (kbd "C-b")     #'klere-fos-battle-map-left)

                                     (define-key mode-map (kbd "<right>") #'klere-fos-battle-map-right)
                                     (define-key mode-map (kbd "f")       #'klere-fos-battle-map-right)
                                     (define-key mode-map (kbd "C-f")     #'klere-fos-battle-map-right)

                                     mode-map)
  "Keymap for `klere-fos--battle-mode-mode'.")

(define-derived-mode klere-fos--battle-mode fundamental-mode "KlereFòs:Battle"
  "Major mode for battle in Klere Fòs."

  (toggle-truncate-lines t)
  (read-only-mode        t))

(defun klere-fos--setup-battle-map-buffer (mapstr pos)
  ""

  (setq klere-fos----battle-matrix (klere-fos--matrix-from-string mapstr))

  (klere-fos--maps-insert klere-fos----battle-matrix)

  (setq klere-fos----battle-position
        (klere-fos--maps-position-insert klere-fos----battle-matrix pos))

  (klere-fos--battle-mode))



(provide 'klere-fos--battle-maps)

;;; klere-fos--battle-maps.el ends here
