;;; klere-fos--town-maps.el --- Klere Fòs: Town Maps                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Klere-F-s
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0
;; Keywords: game, rpg

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(require 'klere-fos--matrices)
(require 'klere-fos--matrix-unit-metadata)
(require 'klere-fos--maps)
;;   if-let*
;; when-let*
(require 'subr-x)

(defun klere-fos-town-map-up ()
  ""
  (interactive)

  (when (or-eq (klere-fos--matrix-tile-type
                 (klere-fos--matrix-get-above-tile klere-fos----town-matrix
                                                   (car klere-fos----town-position)))
               ?N
               ?t)
    (read-only-mode 0)
    (setq klere-fos----town-position (klere-fos--maps-move-cursor-position-up
                                       klere-fos----town-matrix
                                       klere-fos----town-position))
    (read-only-mode 1)))

(defun klere-fos-town-map-down ()
  ""
  (interactive)

  (when (or-eq (klere-fos--matrix-tile-type
                 (klere-fos--matrix-get-below-tile klere-fos----town-matrix
                                                   (car klere-fos----town-position)))
               ?N
               ?t)
    (read-only-mode 0)
    (setq klere-fos----town-position (klere-fos--maps-move-cursor-position-down
                                       klere-fos----town-matrix
                                       klere-fos----town-position))
    (read-only-mode 1)))

(defun klere-fos-town-map-left ()
  ""
  (interactive)

  (when (or-eq (klere-fos--matrix-tile-type
                 (klere-fos--matrix-get-prev-tile klere-fos----town-matrix
                                                  (car klere-fos----town-position)))
               ?N
               ?t)
    (read-only-mode 0)
    (setq klere-fos----town-position (klere-fos--maps-move-cursor-position-left
                                       klere-fos----town-matrix
                                       klere-fos----town-position))
    (read-only-mode 1)))

(defun klere-fos-town-map-right ()
  ""
  (interactive)

  (when (or-eq (klere-fos--matrix-tile-type
                 (klere-fos--matrix-get-next-tile klere-fos----town-matrix
                                                  (car klere-fos----town-position)))
                ?N
                ?t)
    (read-only-mode 0)
    (setq klere-fos----town-position (klere-fos--maps-move-cursor-position-right
                                       klere-fos----town-matrix
                                       klere-fos----town-position))
    (read-only-mode 1)))

(defun klere-fos-town-map-beginning ()
  ""
  (interactive)

  (read-only-mode 0)
  (setq klere-fos----town-position
        (klere-fos--maps-move-cursor-position-to-row-beg klere-fos----town-matrix
                                                         klere-fos----town-position
                                                         '(?N ?t)))
  (read-only-mode 1))

(defun klere-fos-town-map-end ()
  ""
  (interactive)

  (read-only-mode 0)
  (setq klere-fos----town-position
        (klere-fos--maps-move-cursor-position-to-row-end klere-fos----town-matrix
                                                         klere-fos----town-position
                                                         '(?N ?t)))
  (read-only-mode 1))

(defun klere-fos-town-map-top ()
  ""
  (interactive)

  (read-only-mode 0)
  (setq klere-fos----town-position
        (klere-fos--maps-move-cursor-position-to-row-top klere-fos----town-matrix
                                                         klere-fos----town-position
                                                         '(?N ?t)))
  (read-only-mode 1))

(defun klere-fos-town-map-bottom ()
  ""
  (interactive)

  (read-only-mode 0)
  (setq klere-fos----town-position
        (klere-fos--maps-move-cursor-position-to-row-bot klere-fos----town-matrix
                                                         klere-fos----town-position
                                                         '(?N ?t)))
  (read-only-mode 1))

(defun klere-fos-town-map-activate ()
  ""
  (interactive)

  (when-let* ((pos                         (car klere-fos----town-position))
              (meta (klere-fos--mu-metadata-get
                      (klere-fos--matrix-get klere-fos----town-matrix pos))))
    (klere-fos--mu-metadata-activate-next meta klere-fos----town-matrix pos)))

(defvar klere-fos--town-mode-map (let ((mode-map (make-sparse-keymap)))
                                   (define-key mode-map (kbd "<up>")     #'klere-fos-town-map-up)
                                   (define-key mode-map (kbd "p")        #'klere-fos-town-map-up)
                                   (define-key mode-map (kbd "C-p")      #'klere-fos-town-map-up)

                                   (define-key mode-map (kbd "<down>")   #'klere-fos-town-map-down)
                                   (define-key mode-map (kbd "n")        #'klere-fos-town-map-down)
                                   (define-key mode-map (kbd "C-n")      #'klere-fos-town-map-down)

                                   (define-key mode-map (kbd "<left>")   #'klere-fos-town-map-left)
                                   (define-key mode-map (kbd "b")        #'klere-fos-town-map-left)
                                   (define-key mode-map (kbd "C-b")      #'klere-fos-town-map-left)

                                   (define-key mode-map (kbd "<right>")  #'klere-fos-town-map-right)
                                   (define-key mode-map (kbd "f")        #'klere-fos-town-map-right)
                                   (define-key mode-map (kbd "C-f")      #'klere-fos-town-map-right)

                                   (define-key mode-map (kbd "<home>")   #'klere-fos-town-map-beginning)
                                   (define-key mode-map (kbd "a")        #'klere-fos-town-map-beginning)
                                   (define-key mode-map (kbd "C-a")      #'klere-fos-town-map-beginning)

                                   (define-key mode-map (kbd "<end>")    #'klere-fos-town-map-end)
                                   (define-key mode-map (kbd "e")        #'klere-fos-town-map-end)
                                   (define-key mode-map (kbd "C-e")      #'klere-fos-town-map-end)

                                   (define-key mode-map (kbd "C-<home>") #'klere-fos-town-map-top)
                                   (define-key mode-map (kbd ",")        #'klere-fos-town-map-top)
                                   (define-key mode-map (kbd "M-<")      #'klere-fos-town-map-top)

                                   (define-key mode-map (kbd "C-<end>")  #'klere-fos-town-map-bottom)
                                   (define-key mode-map (kbd ".")        #'klere-fos-town-map-bottom)
                                   (define-key mode-map (kbd "M->")      #'klere-fos-town-map-bottom)

                                   (define-key mode-map (kbd "l")        #'klere-fos--maps-center)
                                   (define-key mode-map (kbd "C-l")      #'klere-fos--maps-center)

                                   (define-key mode-map (kbd "SPC")      #'klere-fos-town-map-activate)
                                   (define-key mode-map (kbd "RET")      #'klere-fos-town-map-activate)

                                   mode-map)
  "Keymap for `klere-fos--town-mode-mode'.")

(define-derived-mode klere-fos--town-mode fundamental-mode "KlereFòs:Town"
  "Major mode for when in a town or building in Klere Fòs."

  (toggle-truncate-lines t)
  (read-only-mode        1))

(defun klere-fos--setup-town-map-buffer (matrix pos)
  ""

  (read-only-mode 0)

  (setq klere-fos----town-matrix matrix)

  (klere-fos--maps-insert klere-fos----town-matrix)

  (setq klere-fos----town-position
        (klere-fos--maps-position-insert klere-fos----town-matrix pos))

  (klere-fos--town-mode))

(defmacro klere-fos--town-maps-defun-house-map-transfer (fn-name prev-matrix prev-pos
                                                                 start-pos   next-map
                                                                 &rest       body)
  ""

  (declare (indent 3))

  `(defun ,fn-name (,prev-matrix ,prev-pos)
     (if-let* ((prev-meta (klere-fos--mu-metadata-get
                            (klere-fos--matrix-get ,prev-matrix ,prev-pos)))
               (prev-map  (klere-fos--mu-metadata-house-get-map prev-meta)))
         (klere-fos--setup-town-map-buffer prev-map ,start-pos)
       (let ((pos ,start-pos)
             (map ,next-map))
         (klere-fos--mu-metadata-house-set-map prev-meta map)

         ,@body))))



(provide 'klere-fos--town-maps)

;;; klere-fos--town-maps.el ends here
