;;; klere-fos--level1.el --- Klere Fòs: Level 1                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Klere-F-s
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0
;; Keywords: game, rpg

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(require 'klere-fos--utils)
(require 'klere-fos--matrices)
(require 'klere-fos--matrix-unit-metadata)
(require 'klere-fos--dialog-exposition)
(require 'klere-fos--town-maps)
(require 'klere-fos--game-data)
;; if-let
(require 'subr-x)

(defun klere-fos--level1 ()
  ""

  (let ((name (klere-fos--game-data-get-user-name)))
    (klere-fos--dia-exp-render
      (concat name "? ▼")
      'blank
      (klere-fos--with-curr-buff-lambda _buff
        (klere-fos--dia-exp-render
          (concat name ". ▼")
          'blank
          (klere-fos--with-curr-buff-lambda _buff2
            (klere-fos--dia-exp-render
              (concat name "! ▼")
              'blank
              (klere-fos--with-curr-buff-lambda _buff3
                (klere-fos--dia-exp-render
                  (concat "Oh, thank GNU. Your head hit the wall so "
                          "hard, I could've sworn I heard it crack! ▼")
                  'lily-standing-over
                  (klere-fos--with-curr-buff-lambda _buff4
                    (klere-fos--dia-exp-render
                      (concat "I think that might be enough games, for today; "
                              "don't you think? You should probably get your "
                              "head checked, though. I think I'm going to "
                              "find my parents: they just got back to port "
                              "with a haul and are setting up for market. ▼")
                      'lily-with-kick-ball
                      (klere-fos--with-curr-buff-lambda _buff5
                        (let ((lily   (klere-fos--mu-metadata-create
                                        (klere-fos--mu-metadata-create-person "Lily")
                                        t
                                        t
                                        (klere-fos--mu-metadata-person-lambda matrix pos rerun-p meta
                                            (concat "You should probably get your "
                                                    "head checked. I think I'm "
                                                    "going to find my parents: they "
                                                    "could use some help with work.")
                                          (klere-fos--dia-exp-render
                                            (concat "I think that might be enough games, for today; "
                                                    "don't you think? You should probably get your "
                                                    "head checked, though. I think I'm going to "
                                                    "find my parents: they just got back to port "
                                                    "with a haul and are setting up for market. ▼")
                                            'lily-with-kick-ball
                                            (klere-fos--with-curr-buff-lambda _b
                                              (klere-fos--setup-town-map-buffer matrix pos))))))
                              (house1 (klere-fos--mu-metadata-create
                                        (klere-fos--mu-metadata-create-house)
                                        nil
                                        nil
                                        (klere-fos--mu-metadata-house-lambda matrix pos meta
                                          (klere-fos--level1-house1 matrix pos))))
                              (house2 (klere-fos--mu-metadata-create
                                        (klere-fos--mu-metadata-create-house t)
                                        nil
                                        nil
                                        (klere-fos--mu-metadata-house-lambda matrix pos meta
                                          (message "The house is locked so "
                                                   "this should never happen!"))))
                              (house3 (klere-fos--mu-metadata-create
                                        (klere-fos--mu-metadata-create-house)
                                        nil
                                        nil
                                        (klere-fos--mu-metadata-house-lambda matrix pos meta
                                          (message "Write a method, for a house!"))))
                              (house4 (klere-fos--mu-metadata-create
                                        (klere-fos--mu-metadata-create-house)
                                        nil
                                        nil
                                        (klere-fos--mu-metadata-house-lambda matrix pos meta
                                          (message "Write a method, for a house!"))))
                              (house5 (klere-fos--mu-metadata-create
                                        (klere-fos--mu-metadata-create-house)
                                        nil
                                        nil
                                        (klere-fos--mu-metadata-house-lambda matrix pos meta
                                          (message "Write a method, for a house!"))))
                              (chest1 (klere-fos--mu-metadata-create
                                        (klere-fos--mu-metadata-create-chest '())
                                        nil
                                        nil
                                        (klere-fos--mu-metadata-chest-lambda matrix pos meta
                                          (message "Write a method, for a chest!"))))
                              (npc1   (klere-fos--mu-metadata-create
                                        (klere-fos--mu-metadata-create-person)
                                        nil
                                        nil
                                        (klere-fos--mu-metadata-person-lambda matrix pos rerun-p meta
                                            "Good evening, name! Lovely weather we're having, no?"
                                          (klere-fos--dia-exp-render
                                            (concat "Good evening, name! Lovely weather we're having, no? ▼")
                                            'npc1
                                            (klere-fos--with-curr-buff-lambda _b
                                              (klere-fos--setup-town-map-buffer matrix pos)))))))
                          (klere-fos--setup-town-map-buffer
                            `[[?W ?W             ?W ?F             ?F ?F             ?F             ?F ?M             ?M]
                              [?W ?N             ?N (?N . ,house1) ?F ?N             (?N . ,chest1) ?F ?F             ?M]
                              [?W (?N . ,lily)   ?W ?N             ?F ?N             ?E             ?E ?F             ?F]
                              [?W ?W             ?W (?N . ,npc1)   ?N (?N . ,house2) ?E             ?E (?N . ,house4) ?F]
                              [?W ?W             ?W ?W             ?N ?F             (?N . ,house3) ?E ?N             ?F]
                              [?W (?N . ,house5) ?N ?N             ?N ?N             ?N             ?F ?N             ?E]
                              [?W ?W             ?W ?W             ?W ?N             ?F             ?F ?N             ?F]
                              [?W ?W             ?F ?W             ?W ?N             ?N             ?N ?N             ?F]
                              [?W ?F             ?F ?F             ?W ?W             ?F             ?F ?F             ?F]]
                            '(1 . 2))))))))
              t)))))))



(provide 'klere-fos--level1)

;;; klere-fos--level1.el ends here
