;;; klere-fos--characters.el --- Klere Fòs: Characters                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Klere-F-s
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0
;; Keywords: game, rpg

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(require 'klere-fos--utils)
;; if-let
(require 'subr-x)

(defun klere-fos--characters-create-equipped (hand rings armor shoes)
  ""

  `((:hand  .  ,hand)
    (:rings . ,rings)
    (:armor . ,armor)
    (:shoes . ,shoes)))
(defun klere-fos--characters-create-pc (name          class
                                        base-hp             hp-funct
                                        base-mp             mp-funct
                                        base-att-phys att-phys-funct
                                        base-def-phys def-phys-funct
                                        base-att-magi att-magi-funct
                                        base-def-magi def-magi-funct
                                        base-agility   agility-funct
                                        base-move     starting-level
                                        equipped
                                        &optional     held-items)
  ""

  `((:name            .                           ,name)
    (:class           .                          ,class)
    (:held-items      .                     ,held-items)
    (:equipped        .                       ,equipped)
    (:current-stats   . ((:hp       . ,base-hp)
                         (:mp       . ,base-mp)))
    (:base-stats      . ((:hp       . ,base-hp)
                         (:mp       . ,base-mp)
                         (:att-phys . ,base-att-phys)
                         (:def-phys . ,base-def-phys)
                         (:att-magi . ,base-att-magi)
                         (:def-magi . ,base-def-magi)
                         (:agility  . ,base-agility)
                         (:move     . ,base-move)))
    (:leveling-functs . ((:hp       . ,hp-funct)
                         (:mp       . ,mp-funct)
                         (:att-phys . ,att-phys-funct)
                         (:def-phys . ,def-phys-funct)
                         (:att-magi . ,att-magi-funct)
                         (:def-magi . ,def-magi-funct)
                         (:agility  .  ,agility-funct)))
    (:status          .                              ())
    (:level           .                 ,starting-level)))

(defmacro klere-fos--characters-get-or-replace-pc-data (get-or-replace
                                                        data-key
                                                        desc-lbl)
  ""

  (let ((str (symbol-name data-key)))
    `(defun ,(intern (concat "klere-fos--characters-"
                             (symbol-name get-or-replace)
                             "-"
                             str)) ,(if (eq get-or-replace 'get)
                                        '(character)
                                      '(character new-value))
       ,(if (eq get-or-replace 'get)
            "Fetch the attribute " desc-lbl " from a game CHARACTER."
          "Replace the " desc-lbl " of a game CHARACTER with NEW-VALUE.")

       ,(if (eq get-or-replace 'get)
            `(alist-get ,(intern (concat ":" str)) character)
          `(alist-replace ,(intern (concat ":" str)) new-value character)))))

(klere-fos--characters-get-or-replace-pc-data get     name "Name")
(klere-fos--characters-get-or-replace-pc-data replace name "Name")

(klere-fos--characters-get-or-replace-pc-data get     class "Class")
(klere-fos--characters-get-or-replace-pc-data replace class "Class")

(klere-fos--characters-get-or-replace-pc-data get     held-items "Held Items")
(klere-fos--characters-get-or-replace-pc-data replace held-items "Held Items")
(defun klere-fos--characters-add-item-to-held (character new-item)
  ""

  (klere-fos--characters-replace-held-items character
                                            (cons new-item
                                                  (klere-fos--characters-get-held-items character))))
(defun klere-fos--characters-add-items-to-held (character new-items)
  ""

  (klere-fos--characters-replace-held-items character
                                            (append new-items
                                                    (klere-fos--characters-get-held-items character))))
(defun klere-fos--characters-remove-item-from-held (character current-item)
  ""

  (klere-fos--characters-replace-held-items character
                                            (remove current-item
                                                    (klere-fos--characters-get-held-items character))))

(klere-fos--characters-get-or-replace-pc-data get     equipped "Equipped")
(klere-fos--characters-get-or-replace-pc-data replace equipped "Equipped")

(klere-fos--characters-get-or-replace-pc-data get     current-stats "Current Stat.s")
(klere-fos--characters-get-or-replace-pc-data replace current-stats "Current Stat.s")

(klere-fos--characters-get-or-replace-pc-data get     base-stats "Base Stat.s")
(klere-fos--characters-get-or-replace-pc-data replace base-stats "Base Stat.s")

(klere-fos--characters-get-or-replace-pc-data get     leveling-functs "Leveling Functions")

(klere-fos--characters-get-or-replace-pc-data get     status "Status Effects")
(klere-fos--characters-get-or-replace-pc-data replace status "Status Effects")
(defun klere-fos--characters-add-status-effect (character new-effect)
  ""

  (let ((status (klere-fos--characters-get-status character)))
    (if (memq new-effect status)
        character
      (klere-fos--characters-replace-status character (cons new-effect status)))))
(defun klere-fos--characters-remove-status-effect (character current-effect)
  ""

  (klere-fos--characters-replace-status character
                                        (remove current-effect
                                                (klere-fos--characters-get-status character))))

(klere-fos--characters-get-or-replace-pc-data get     level "Level")
(klere-fos--characters-get-or-replace-pc-data replace level "Level")

(defmacro klere-fos--characters-get-stat-or-leveling-funct (stat-or-funct
                                                            desc-lbl
                                                            stat-p
                                                            &optional current-p)
  ""

  (let ((str (symbol-name stat-or-funct)))
    `(defun ,(intern (concat "klere-fos--characters-get-"
                             (when   stat-p (if current-p "current-" "base-"))
                             str
                             (unless stat-p "-funct"))) (,(if stat-p
                                                              'char-or-stats
                                                            'char))
       ,(if stat-p
            (concat "Get the " (if current-p "current" "base") " statistic of "
                    desc-lbl " from a character."
                    "\n"
                    "\n"
                    "Given character data via CHAR-OR-STATS, the "
                    (if current-p "current" "base") " " desc-lbl " for the\n"
                    "character is looked up and returned."
                    "\n"
                    "\n"
                    "Notably, this does not have to be from a character's "
                    "info. and can be the\nstatistics of any character data "
                    "(meaning it is an alist which includes the key\n':"
                    (if current-p "current-" "base-") "stats'.")
          (concat "Get the leveling function of " desc-lbl " from a character."
                  "\n"
                  "\n"
                  "Given character data via CHAR, the function to compute the "
                  "character's value for\n" desc-lbl " at zir next level is "
                  "looked up and returned."
                  "\n"
                  "\n"
                  "The leveling funtion, given a level and current statistic "
                  "value, determines what\nsaid statistic should be, at the "
                  "next level, for the character."))

       (alist-get ,(intern (concat ":" str))
                  ,(if stat-p
                       `(if-let ((stats (alist-get ,(if current-p
                                                        :current-stats
                                                      :base-stats) char-or-stats)))
                            stats
                          char-or-stats)
                     `(klere-fos--characters-get-leveling-functs char))))))

(klere-fos--characters-get-stat-or-leveling-funct hp         "Hit Points"     t   t)
(klere-fos--characters-get-stat-or-leveling-funct hp         "Hit Points"     t)
(klere-fos--characters-get-stat-or-leveling-funct hp         "Hit Points"     nil)
(klere-fos--characters-get-stat-or-leveling-funct mp       "Magic Points"     t   t)
(klere-fos--characters-get-stat-or-leveling-funct mp       "Magic Points"     t)
(klere-fos--characters-get-stat-or-leveling-funct mp       "Magic Points"     nil)
(klere-fos--characters-get-stat-or-leveling-funct att-phys "Physical Attack"  t)
(klere-fos--characters-get-stat-or-leveling-funct att-phys "Physical Attack"  nil)
(klere-fos--characters-get-stat-or-leveling-funct def-phys "Physical Defense" t)
(klere-fos--characters-get-stat-or-leveling-funct def-phys "Physical Defense" nil)
(klere-fos--characters-get-stat-or-leveling-funct att-magi  "Magical Attack"  t)
(klere-fos--characters-get-stat-or-leveling-funct att-magi  "Magical Attack"  nil)
(klere-fos--characters-get-stat-or-leveling-funct def-magi  "Magical Defense" t)
(klere-fos--characters-get-stat-or-leveling-funct def-magi  "Magical Defense" nil)
(klere-fos--characters-get-stat-or-leveling-funct agility   "Agility"         t)
(klere-fos--characters-get-stat-or-leveling-funct agility   "Agility"         nil)
(klere-fos--characters-get-stat-or-leveling-funct move     "Mobility"         t)

(defun klere-fos--characters-compute-next-level (char)
  ""

  (let ((lvl (1+ (klere-fos--characters-get-level char))))
    (klere-fos--characters-replace-base-stats
      (klere-fos--characters-replace-level char lvl)
      `((:hp       . ,(funcall (klere-fos--characters-get-hp-funct char)
                               lvl
                               (klere-fos--characters-get-base-hp  char)))
        (:mp       . ,(funcall (klere-fos--characters-get-mp-funct char)
                               lvl
                               (klere-fos--characters-get-base-mp  char)))
        (:att-phys . ,(funcall (klere-fos--characters-get-att-phys-funct char)
                               lvl
                               (klere-fos--characters-get-base-att-phys  char)))
        (:def-phys . ,(funcall (klere-fos--characters-get-def-phys-funct char)
                               lvl
                               (klere-fos--characters-get-base-def-phys  char)))
        (:att-magi . ,(funcall (klere-fos--characters-get-att-magi-funct char)
                               lvl
                               (klere-fos--characters-get-base-att-magi  char)))
        (:def-magi . ,(funcall (klere-fos--characters-get-def-magi-funct char)
                               lvl
                               (klere-fos--characters-get-base-def-magi  char)))
        (:agility  . ,(funcall (klere-fos--characters-get-agility-funct char)
                               lvl
                               (klere-fos--characters-get-base-agility  char)))
        (:move     . ,(klere-fos--characters-get-base-move char))))))

(let* ((     purely-steady    (lambda (lvl curr-val)
                                (+ curr-val (pcase lvl
                                              ((pred (> 21) _) 1)
                                              ((pred (> 31) _) 2)
                                              ((pred (> 41) _) 3)))))
       (oscillating-steady    (lambda (lvl curr-val)
                                (if (zerop curr-val)
                                    0
                                  (+ curr-val (pcase (random 100)
                                                (0              (pcase lvl
                                                                  ((pred (> 21) _) 3)
                                                                  ((pred (> 31) _) 4)
                                                                  ((pred (> 41) _) 5)))
                                                ((pred oddp  _) (pcase lvl
                                                                  ((pred (> 21) _) 1)
                                                                  ((pred (> 31) _) 2)
                                                                  ((pred (> 41) _) 3)))
                                                ((pred evenp _) (pcase lvl
                                                                  ((pred (> 21) _) 2)
                                                                  ((pred (> 31) _) 3)
                                                                  ((pred (> 41) _) 4))))))))
       (     purely-expanding (lambda (lvl curr-val)
                                (ceiling (* curr-val 1.045))))
       (oscillating-expanding (lambda (lvl curr-val)
                                (ceiling (* curr-val
                                            (pcase (random 100)
                                              (0              1)
                                              ((pred oddp  _) 1.045)
                                              ((pred evenp _) 1.090))))))
       (     purely-shrinking (lambda (lvl curr-val)
                                (round (+ (* 0.868104
                                             (expt curr-val 1.01869))
                                          (if (< lvl 21)
                                              5.5147
                                            6.5147)))))
       (     purely-mixed     (lambda (lvl curr-val)
                                (funcall (pcase lvl
                                           ((pred (> 18) _) purely-shrinking)
                                           ((pred (< 24) _) purely-steady)
                                           (_               purely-expanding))
                                         lvl
                                         curr-val))))
  (defvar klere-fos--characters-main
          (lambda (name)
            (klere-fos--characters-create-pc name
                                             'swordsperson
                                             10 oscillating-steady
                                             00 oscillating-steady
                                             12 oscillating-steady
                                             04 oscillating-steady
                                             06 oscillating-steady
                                             05 oscillating-steady
                                             04 oscillating-steady
                                             06
                                             01
                                             (klere-fos--characters-create-equipped
                                               klere-fos----items-wooden-sword
                                               '()
                                               nil
                                               nil)))
    "")
  (defvar klere-fos----characters-hiwe
          (klere-fos--characters-create-pc "Hiwe"
                                           'vicar-forane
                                           11 purely-steady
                                           10 purely-expanding
                                           04 purely-steady
                                           05 purely-steady
                                           13 purely-steady
                                           11 purely-steady
                                           05 purely-steady
                                           05
                                           01
                                           (klere-fos--characters-create-equipped nil
                                                                                  '()
                                                                                  nil
                                                                                  nil))
    "")
  (defvar klere-fos----characters-lik
          (klere-fos--characters-create-pc "Lik"
                                           'dwarf
                                           09      purely-steady
                                           00 oscillating-expanding
                                           14      purely-steady
                                           07 oscillating-expanding
                                           15 oscillating-expanding
                                           07 oscillating-steady
                                           04      purely-steady
                                           06
                                           01
                                           (klere-fos--characters-create-equipped nil
                                                                                  '()
                                                                                  nil
                                                                                  nil))
    "")
  (defvar klere-fos----characters-rosaline
          (klere-fos--characters-create-pc "Rosaline"
                                           'bishop
                                           11      purely-steady
                                           07 oscillating-steady
                                           08      purely-steady
                                           04 oscillating-steady
                                           12 oscillating-steady
                                           10 oscillating-steady
                                           08      purely-steady
                                           05
                                           01
                                           (klere-fos--characters-create-equipped nil
                                                                                  '()
                                                                                  nil
                                                                                  nil))
    ""))



(provide 'klere-fos--characters)

;;; klere-fos--characters.el ends here
