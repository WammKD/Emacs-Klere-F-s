;;; klere-fos--maps.el --- Klere Fòs: Maps                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Klere-F-s
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0
;; Keywords: game, rpg

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(require 'klere-fos--matrices)
(require 'klere-fos--matrix-unit-metadata)
;; pcase
(require 'pcase)

(defun klere-fos--maps-pos-to-col (pos-or-x)
  "Move to the column, in the buffer, which corresponds to the top-right corner
of the tile which belongs do the coordinate you want to target.

POS-OR-X is either a `cons' of the coordinate or just the integer of the X value
of the coordinate."

  (* (if (consp pos-or-x) (car pos-or-x) pos-or-x) 4))

(defun klere-fos--maps-pos-to-line (pos-or-y)
  "Move to the line, in the buffer, which corresponds to the top-right corner of
the tile which belongs do the coordinate you want to target.

POS-OR-Y is either a `cons' of the coordinate or just the integer of the Y value
of the coordinate."

  (1+ (* (if (consp pos-or-y) (car pos-or-y) pos-or-y) 4)))



(defun klere-fos--generate-castle-wall-tile (matrix pos)
  ""

  (let ((prop (lambda (str)
                (propertize str 'face '(:foreground "dimgray")))))
    (cond
     ((and (not (eq (klere-fos--matrix-get-above-tile matrix pos) ?B))
                (eq (klere-fos--matrix-get-next-tile  matrix pos) ?B)
                (eq (klere-fos--matrix-get-below-tile matrix pos) ?B))
        (concat               "     "  "\n"
                (funcall prop " ╦╦╦╦") "\n"
                (funcall prop " ╠╣╠╣") "\n"
                (funcall prop " ╣╠╣║") "\n"
                (funcall prop " ╠╣╠╣")))
     ((and (not (eq (klere-fos--matrix-get-next-tile       matrix pos) ?B))
                (eq (klere-fos--matrix-get-below-tile      matrix pos) ?B)
                (eq (klere-fos--matrix-get-above-tile      matrix pos) ?B)
                (eq (klere-fos--matrix-get-prev-above-tile matrix pos) ?B))
        (concat (funcall prop "╩╦╣╠╣") "\n"
                (funcall prop " ╣╠╣║") "\n"
                (funcall prop " ╠╣╠╣") "\n"
                (funcall prop " ╣╠╣║") "\n"
                (funcall prop " ╠╣╠╣")))
     ((and (not (eq (klere-fos--matrix-get-next-tile  matrix pos) ?B))
                (eq (klere-fos--matrix-get-below-tile matrix pos) ?B)
                (eq (klere-fos--matrix-get-above-tile matrix pos) ?B))
        (concat (funcall prop " ╠╣╠╣") "\n"
                (funcall prop " ╣╠╣║") "\n"
                (funcall prop " ╠╣╠╣") "\n"
                (funcall prop " ╣╠╣║") "\n"
                (funcall prop " ╠╣╠╣")))
     ((and (not (eq (klere-fos--matrix-get-next-above-tile matrix pos) ?B))
                (eq (klere-fos--matrix-get-next-tile       matrix pos) ?B)
                (eq (klere-fos--matrix-get-above-tile      matrix pos) ?B))
        (concat (funcall prop " ╠╣╠╣") "\n"
                (funcall prop " ╣╠╣╠") "\n"
                (funcall prop " ╠╣╠╩") "\n"
                (funcall prop " ╣╠╩╦") "\n"
                              " ╠╩╦╩"))
     ((and (not (eq (klere-fos--matrix-get-above-tile       matrix pos) ?B))
                (eq (klere-fos--matrix-get-next-tile        matrix pos) ?B)
                (eq (klere-fos--matrix-get-prev-tile        matrix pos) ?B)
                (eq (klere-fos--matrix-get-prev-above-tile  matrix pos) ?B))
        (concat (funcall prop "╣    ") "\n"
                (funcall prop "╠═╦═╦") "\n"
                (funcall prop "╩╦╩╦╩") "\n"
                (funcall prop "╦╩╦╩╦") "\n"
                (funcall prop "╩╦╩╦╩")))
     ((and (not (eq (klere-fos--matrix-get-above-tile      matrix pos) ?B))
                (eq (klere-fos--matrix-get-next-tile       matrix pos) ?B)
                (eq (klere-fos--matrix-get-prev-tile       matrix pos) ?B)
                (eq (klere-fos--matrix-get-prev-below-tile matrix pos) ?B))
        (concat               "     "  "\n"
                (funcall prop "╦═╦═╦") "\n"
                (funcall prop "╣╦╩╦╩") "\n"
                (funcall prop "║╩╦╩╦") "\n"
                (funcall prop "╣╦╩╦╩")))
     ((and (not (eq (klere-fos--matrix-get-above-tile matrix pos) ?B))
                (eq (klere-fos--matrix-get-next-tile  matrix pos) ?B)
                (eq (klere-fos--matrix-get-prev-tile  matrix pos) ?B))
        (concat               "     "  "\n"
                (funcall prop "╦═╦═╦") "\n"
                (funcall prop "╩╦╩╦╩") "\n"
                (funcall prop "╦╩╦╩╦") "\n"
                (funcall prop "╩╦╩╦╩")))
     ((and (not (eq (klere-fos--matrix-get-prev-above-tile  matrix pos) ?B))
           (not (eq (klere-fos--matrix-get-next-tile        matrix pos) ?B))
                (eq (klere-fos--matrix-get-prev-tile        matrix pos) ?B)
                (eq (klere-fos--matrix-get-above-tile       matrix pos) ?B))
        (concat (funcall prop " ╠╣╠╣") "\n"
                (funcall prop "╦═╦═╣") "\n"
                (funcall prop "╩╦╩╦╣") "\n"
                (funcall prop "╦╩╦╩╣") "\n"
                (funcall prop "╩╦╩╦╣")))
     ((and (not (eq (klere-fos--matrix-get-next-tile  matrix pos) ?B))
                (eq (klere-fos--matrix-get-above-tile matrix pos) ?B)
                (eq (klere-fos--matrix-get-below-tile matrix pos) ?B))
        (concat " " (funcall prop
                             (if (eq (klere-fos--matrix-get-prev-above-tile matrix
                                                                            pos)
                                     ?B)
                                 "╦"
                               "╠"))
                    (funcall prop "╣╠╣") "\n"
                (funcall prop " ╣╠╣║") "\n"
                (funcall prop " ╠╣╠╣") "\n"
                (funcall prop " ╣╠╣║") "\n"
                (funcall prop " ╠╣╠╣")))
     ((and (not (eq (klere-fos--matrix-get-above-tile matrix pos) ?B))
           (not (eq (klere-fos--matrix-get-next-tile  matrix pos) ?B))
                (eq (klere-fos--matrix-get-prev-tile  matrix pos) ?B)
                (eq (klere-fos--matrix-get-below-tile matrix pos) ?B))
        (concat               "     "  "\n"
                (funcall prop "╦═╦═╗") "\n"
                (funcall prop "╩╦╩╦╣") "\n"
                (funcall prop "╦╩╦╣║") "\n"
                (funcall prop "╩╦╣╠╣"))))))

(defun klere-fos--generate-river-tile (matrix pos)
  ""

  (let* ((prop     (lambda (str)
                     (propertize str 'face '(:foreground "blue"))))
         (   genfn (lambda ()
                     (funcall prop (if (zerop (random 2)) "≈" "="))))
         (borderfn (lambda (placement)
                     (cond
                      ((and (= (length placement) 1)
                            (or-eq (funcall (pcase placement
                                              ('(top)    #'klere-fos--matrix-get-above-tile)
                                              ('(left)   #'klere-fos--matrix-get-prev-tile)
                                              ('(right)  #'klere-fos--matrix-get-next-tile)
                                              ('(bottom) #'klere-fos--matrix-get-below-tile))
                                            matrix
                                            pos) ?W nil))
                         (funcall genfn))
                      ((equal placement '(top left))
                         (cond
                          ((and (or-eq (klere-fos--matrix-get-prev-above-tile matrix pos) ?W nil)
                                (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil)
                                (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil))
                             (funcall genfn))
                          ((and      (or-eq (klere-fos--matrix-get-prev-above-tile matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil)))
                             (funcall prop "╬"))
                          ((and      (or-eq (klere-fos--matrix-get-prev-above-tile matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil))
                             (funcall prop "╗"))
                          ((and      (or-eq (klere-fos--matrix-get-prev-above-tile matrix pos) ?W nil)
                                     (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil)))
                             (funcall prop "╚"))
                          ((and (not (or-eq (klere-fos--matrix-get-prev-above-tile matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil))
                             (funcall prop "║"))
                          ((and (not (or-eq (klere-fos--matrix-get-prev-above-tile matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil)))
                             (funcall prop "═"))
                          ((and (not (or-eq (klere-fos--matrix-get-prev-above-tile matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil)
                                     (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil))
                             (funcall prop "╝"))
                          ((and (not (or-eq (klere-fos--matrix-get-prev-above-tile matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil)))
                             (funcall prop "╔"))))
                      ((equal placement '(top right))
                         (cond
                          ((and (or-eq (klere-fos--matrix-get-next-above-tile matrix pos) ?W nil)
                                (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil)
                                (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil))
                             (funcall genfn))
                          ((and      (or-eq (klere-fos--matrix-get-next-above-tile matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil)))
                             (funcall prop "╬"))
                          ((and      (or-eq (klere-fos--matrix-get-next-above-tile matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil))
                             (funcall prop "╔"))
                          ((and      (or-eq (klere-fos--matrix-get-next-above-tile matrix pos) ?W nil)
                                     (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil)))
                             (funcall prop "╝"))
                          ((and (not (or-eq (klere-fos--matrix-get-next-above-tile matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil))
                             (funcall prop "║"))
                          ((and (not (or-eq (klere-fos--matrix-get-next-above-tile matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil)))
                             (funcall prop "═"))
                          ((and (not (or-eq (klere-fos--matrix-get-next-above-tile matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil)
                                     (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil))
                             (funcall prop "╚"))
                          ((and (not (or-eq (klere-fos--matrix-get-next-above-tile matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?W nil)))
                             (funcall prop "╗"))))
                      ((equal placement '(right bottom))
                         (cond
                          ((and (or-eq (klere-fos--matrix-get-next-below-tile matrix pos) ?W nil)
                                (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil)
                                (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil))
                             (funcall genfn))
                          ((and      (or-eq (klere-fos--matrix-get-next-below-tile matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil)))
                             (funcall prop "╬"))
                          ((and      (or-eq (klere-fos--matrix-get-next-below-tile matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil))
                             (funcall prop "╚"))
                          ((and      (or-eq (klere-fos--matrix-get-next-below-tile matrix pos) ?W nil)
                                     (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil)))
                             (funcall prop "╗"))
                          ((and (not (or-eq (klere-fos--matrix-get-next-below-tile matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil))
                             (funcall prop "║"))
                          ((and (not (or-eq (klere-fos--matrix-get-next-below-tile matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil)))
                             (funcall prop "═"))
                          ((and (not (or-eq (klere-fos--matrix-get-next-below-tile matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil)
                                     (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil))
                             (funcall prop "╔"))
                          ((and (not (or-eq (klere-fos--matrix-get-next-below-tile matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil)))
                             (funcall prop "╝"))))
                      ((equal placement '(left bottom))
                         (cond
                          ((and (or-eq (klere-fos--matrix-get-prev-below-tile matrix pos) ?W nil)
                                (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil)
                                (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil))
                             (funcall genfn))
                          ((and      (or-eq (klere-fos--matrix-get-prev-below-tile matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil)))
                             (funcall prop "╬"))
                          ((and      (or-eq (klere-fos--matrix-get-prev-below-tile matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil))
                             (funcall prop "╝"))
                          ((and      (or-eq (klere-fos--matrix-get-prev-below-tile matrix pos) ?W nil)
                                     (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil)))
                             (funcall prop "╔"))
                          ((and (not (or-eq (klere-fos--matrix-get-prev-below-tile matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil))
                             (funcall prop "║"))
                          ((and (not (or-eq (klere-fos--matrix-get-prev-below-tile matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil)
                                (not (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil)))
                             (funcall prop "═"))
                          ((and (not (or-eq (klere-fos--matrix-get-prev-below-tile matrix pos) ?W nil))
                                     (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil)
                                     (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil))
                             (funcall prop "╗"))
                          ((and (not (or-eq (klere-fos--matrix-get-prev-below-tile matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?W nil))
                                (not (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?W nil)))
                             (funcall prop "╚"))))
                      (t (if (or-eq (lambda (place)
                                      (member place placement)) 'top 'bottom)
                             (funcall prop "═")
                           (funcall prop "║"))))))
         (   3x3fn (lambda ()
                     (concat (funcall genfn) (funcall genfn) (funcall genfn)))))
    (concat (funcall borderfn '(top left)) (funcall borderfn '(top)) (funcall borderfn '(top))
                                           (funcall borderfn '(top)) (funcall borderfn '(top right))
            "\n"
            (funcall borderfn '(left)) (funcall 3x3fn) (funcall borderfn '(right))
            "\n"
            (funcall borderfn '(left)) (funcall 3x3fn) (funcall borderfn '(right))
            "\n"
            (funcall borderfn '(left)) (funcall 3x3fn) (funcall borderfn '(right))
            "\n"
            (funcall borderfn '(left bottom)) (funcall borderfn '(bottom)) (funcall borderfn '(bottom))
                                              (funcall borderfn '(bottom)) (funcall borderfn '(right bottom)))))

(defun klere-fos--generate-forest-tile (matrix pos)
  ""

  (let* ((prop     (lambda (str)
                     (propertize str 'face '(:foreground "dark green"))))
         (   genfn (lambda ()
                     (funcall prop (if (zerop (random 2)) "♠" "♣"))))
         (borderfn (lambda (placement)
                     (cond
                      ((or (and (= (length placement) 1)
                                (or-eq (funcall (pcase placement
                                                  ('(top)    #'klere-fos--matrix-get-above-tile)
                                                  ('(left)   #'klere-fos--matrix-get-prev-tile)
                                                  ('(right)  #'klere-fos--matrix-get-next-tile)
                                                  ('(bottom) #'klere-fos--matrix-get-below-tile))
                                                matrix
                                                pos) ?F nil))
                           (and (equal placement '(top left))
                                (or-eq (klere-fos--matrix-get-prev-above-tile matrix pos) ?F nil)
                                (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?F nil)
                                (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?F nil))
                           (and (equal placement '(top right))
                                (or-eq (klere-fos--matrix-get-next-above-tile matrix pos) ?F nil)
                                (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?F nil)
                                (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?F nil))
                           (and (equal placement '(left bottom))
                                (or-eq (klere-fos--matrix-get-prev-below-tile matrix pos) ?F nil)
                                (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?F nil)
                                (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?F nil))
                           (and (equal placement '(right bottom))
                                (or-eq (klere-fos--matrix-get-next-below-tile matrix pos) ?F nil)
                                (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?F nil)
                                (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?F nil)))
                         (funcall genfn))
                      (t " "))))
         (   3x3fn (lambda ()
                     (concat (funcall genfn) (funcall genfn) (funcall genfn)))))
    (concat (funcall borderfn '(top left)) (funcall borderfn '(top)) (funcall borderfn '(top))
                                           (funcall borderfn '(top)) (funcall borderfn '(top right))
            "\n"
            (funcall borderfn '(left)) (funcall 3x3fn) (funcall borderfn '(right))
            "\n"
            (funcall borderfn '(left)) (funcall 3x3fn) (funcall borderfn '(right))
            "\n"
            (funcall borderfn '(left)) (funcall 3x3fn) (funcall borderfn '(right))
            "\n"
            (funcall borderfn '(left bottom)) (funcall borderfn '(bottom)) (funcall borderfn '(bottom))
                                              (funcall borderfn '(bottom)) (funcall borderfn '(right bottom)))))

(defun klere-fos--generate-mountain-tile (matrix pos)
  ""

  (let* ((   genfn (lambda ()
                     (if (zerop (random 2))
                         (propertize "⛰" 'face '(:foreground "saddlebrown"))
                       (propertize "^" 'face '(:foreground "darkgoldenrod")))))
         (borderfn (lambda (placement)
                     (cond
                      ((or (and (= (length placement) 1)
                                (or-eq (funcall (pcase placement
                                                  ('(top)    #'klere-fos--matrix-get-above-tile)
                                                  ('(left)   #'klere-fos--matrix-get-prev-tile)
                                                  ('(right)  #'klere-fos--matrix-get-next-tile)
                                                  ('(bottom) #'klere-fos--matrix-get-below-tile))
                                                matrix
                                                pos) ?M nil))
                           (and (equal placement '(top left))
                                (or-eq (klere-fos--matrix-get-prev-above-tile matrix pos) ?M nil)
                                (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?M nil)
                                (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?M nil))
                           (and (equal placement '(top right))
                                (or-eq (klere-fos--matrix-get-next-above-tile matrix pos) ?M nil)
                                (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?M nil)
                                (or-eq (klere-fos--matrix-get-above-tile      matrix pos) ?M nil))
                           (and (equal placement '(left bottom))
                                (or-eq (klere-fos--matrix-get-prev-below-tile matrix pos) ?M nil)
                                (or-eq (klere-fos--matrix-get-prev-tile       matrix pos) ?M nil)
                                (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?M nil))
                           (and (equal placement '(right bottom))
                                (or-eq (klere-fos--matrix-get-next-below-tile matrix pos) ?M nil)
                                (or-eq (klere-fos--matrix-get-next-tile       matrix pos) ?M nil)
                                (or-eq (klere-fos--matrix-get-below-tile      matrix pos) ?M nil)))
                         (funcall genfn))
                      (t " "))))
         (   3x3fn (lambda ()
                     (concat (funcall genfn) (funcall genfn) (funcall genfn)))))
    (concat (funcall borderfn '(top left)) (funcall borderfn '(top)) (funcall borderfn '(top))
                                           (funcall borderfn '(top)) (funcall borderfn '(top right))
            "\n"
            (funcall borderfn '(left)) (funcall 3x3fn) (funcall borderfn '(right))
            "\n"
            (funcall borderfn '(left)) (funcall 3x3fn) (funcall borderfn '(right))
            "\n"
            (funcall borderfn '(left)) (funcall 3x3fn) (funcall borderfn '(right))
            "\n"
            (funcall borderfn '(left bottom)) (funcall borderfn '(bottom)) (funcall borderfn '(bottom))
                                              (funcall borderfn '(bottom)) (funcall borderfn '(right bottom)))))

(defun klere-fos--generate-walkable-tile-icon (matrix pos)
  ""

  (if-let* ((meta (klere-fos--mu-metadata-get (klere-fos--matrix-get matrix pos))))
      (if (not (klere-fos--mu-metadata-revealed-p meta))
          (propertize "?" 'face '(:weight bold))
        (pcase (klere-fos--mu-metadata-get-type meta)
          ('Bookcase (if (or-eq (klere-fos--mu-metadata-bookcase-position-get meta)
                                'one
                                'three)
                         (propertize "Ħ" 'face '(:foreground "#B85C00"))
                       " "))
          ('Person   "ᑍ")
          ('House  (if (klere-fos--mu-metadata-house-locked-p meta)
                       (propertize "⚿" 'face '(:foreground "#CD7F32"))
                     "⛶"))
          ('Chest  (if (klere-fos--mu-metadata-chest-locked-p meta)
                       (propertize "⚿" 'face '(:foreground "#CD7F32"))
                     (propertize "¢" 'face '(:foreground "#FFDE34"))))
          (_       " ")))
    " "))
(defun klere-fos--generate-navigate-tile (matrix pos)
  ""

  (let ((borderfn (lambda (placement)
                    (pcase placement
                      ('(top middle)
                         (if (eq (klere-fos--matrix-tile-type
                                   (klere-fos--matrix-get-above-tile matrix
                                                                     pos))    ?N)
                             "│"
                           " "))
                      ('(left middle)
                         (if (eq (klere-fos--matrix-tile-type
                                   (klere-fos--matrix-get-prev-tile matrix
                                                                    pos))    ?N)
                             "─"
                           " "))
                      ('(right  middle)
                         (if (eq (klere-fos--matrix-tile-type
                                   (klere-fos--matrix-get-next-tile matrix
                                                                    pos))   ?N)
                             "─"
                           " "))
                      ('(bottom middle)
                         (if (eq (klere-fos--matrix-tile-type
                                   (klere-fos--matrix-get-below-tile matrix
                                                                     pos))   ?N)
                             "│"
                           " "))
                      (_ " "))))
        ( sidesfn (lambda (placement)
                    (let ((type (klere-fos--mu-metadata-get-type
                                  (klere-fos--mu-metadata-get
                                    (klere-fos--matrix-get matrix pos)))))
                      (pcase placement
                        ('(top)
                           (pcase type
                             ('House (propertize "\\" 'face '(:foreground "#9F0F02")))
                             (_      (if (eq (klere-fos--matrix-tile-type
                                               (klere-fos--matrix-get-above-tile matrix
                                                                                 pos))
                                             ?N) "┴" "─"))))
                        ('(left)
                           (propertize
                             (if (eq (klere-fos--matrix-tile-type
                                       (klere-fos--matrix-get-prev-tile matrix
                                                                        pos))   ?N)
                                 "┤"
                               "│")
                             'face
                             `(:foreground ,(pcase type
                                              ('House "#7E6020")
                                              (_            nil)))))
                        ('(right)
                           (propertize
                             (if (eq (klere-fos--matrix-tile-type
                                       (klere-fos--matrix-get-next-tile matrix
                                                                        pos))   ?N)
                                 "├"
                               "│")
                             'face
                             `(:foreground ,(pcase type
                                              ('House "#7E6020")
                                              (_            nil)))))
                        ('(bottom)
                           (propertize
                             (if (eq (klere-fos--matrix-tile-type
                                       (klere-fos--matrix-get-below-tile matrix
                                                                         pos))   ?N)
                                 "┬"
                               "─")
                             'face
                             `(:foreground ,(pcase type
                                              ('House "#7E6020")
                                              (_            nil)))))
                        ('(top left)
                           (pcase type
                             ('House (propertize "/" 'face '(:foreground "#9F0F02")))
                             (_      "┌")))
                        ('(top right)
                           (pcase type
                             ('House (propertize "\\" 'face '(:foreground "#9F0F02")))
                             (_      "┐")))
                        ('(left  bottom)
                           (propertize "└" 'face `(:foreground ,(pcase type
                                                                  ('House "#7E6020")
                                                                  (_            nil)))))
                        ('(right bottom)
                           (propertize "┘" 'face `(:foreground ,(pcase type
                                                                  ('House "#7E6020")
                                                                  (_            nil))))))))))
    (concat (funcall borderfn '(top left)) (funcall borderfn '(top)) (funcall borderfn '(top middle))
                                           (funcall borderfn '(top)) (funcall borderfn '(top right))
            "\n"
            (funcall borderfn '(left)) (funcall sidesfn '(top left))
                                       (funcall sidesfn '(top))
                                       (funcall sidesfn '(top right)) (funcall borderfn '(right))
            "\n"
            (funcall borderfn '(left middle)) (funcall sidesfn '(left))
                                              (klere-fos--generate-walkable-tile-icon matrix pos)
                                              (funcall sidesfn '(right)) (funcall borderfn '(right middle))
            "\n"
            (funcall borderfn '(left)) (funcall sidesfn '(left  bottom))
                                       (funcall sidesfn '(bottom))
                                       (funcall sidesfn '(right bottom)) (funcall borderfn '(right))
            "\n"
            (funcall borderfn '(left bottom)) (funcall borderfn '(bottom)) (funcall borderfn '(bottom middle))
                                              (funcall borderfn '(bottom)) (funcall borderfn '(right bottom)))))
(defun klere-fos--generate-house-floor-tile (matrix pos)
  ""

  (let* ((char     (propertize "Ħ" 'face '(:foreground "#B85C00")))
         (borderfn (lambda (placement)
                     (let ((type (klere-fos--mu-metadata-get-type
                                   (klere-fos--mu-metadata-get
                                     (klere-fos--matrix-get matrix pos)))))
                       (pcase placement
                         ('(top)
                            (if (eq (klere-fos--matrix-tile-type
                                      (klere-fos--matrix-get-above-tile matrix
                                                                        pos))    ?t)
                                "║"
                              "╥"))
                         ('(top middle)
                            (if (eq (klere-fos--matrix-tile-type
                                      (klere-fos--matrix-get-above-tile matrix
                                                                        pos))    ?t)
                                " "
                              "─"))
                         ('(left)
                            (if (eq (klere-fos--matrix-tile-type
                                      (klere-fos--matrix-get-prev-tile matrix
                                                                       pos))    ?t)
                                "═"
                              "╞"))
                         ('(left middle)
                            (if (eq (klere-fos--matrix-tile-type
                                      (klere-fos--matrix-get-prev-tile matrix
                                                                       pos))    ?t)
                                " "
                              "│"))
                         ('(right )
                            (if (eq (klere-fos--matrix-tile-type
                                      (klere-fos--matrix-get-next-tile matrix
                                                                       pos))    ?t)
                                "═"
                              "╡"))
                         ('(right  middle)
                            (if (eq (klere-fos--matrix-tile-type
                                      (klere-fos--matrix-get-next-tile matrix
                                                                       pos))    ?t)
                                " "
                              "│"))
                         ('(bottom)
                            (if (eq (klere-fos--matrix-tile-type
                                      (klere-fos--matrix-get-below-tile matrix
                                                                        pos))   ?t)
                                "║"
                              (pcase type
                                ('Exit "┈")
                                (_     "╨"))))
                         ('(bottom middle)
                            (if (eq (klere-fos--matrix-tile-type
                                      (klere-fos--matrix-get-below-tile matrix
                                                                        pos))   ?t)
                                " "
                              (pcase type
                                ('Exit "┈")
                                (_     "─"))))
                         ('(top left)
                            (cond
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-prev-above-tile matrix
                                                                                pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-above-tile      matrix
                                                                                pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-prev-tile       matrix
                                                                                pos))   ?t))
                                   " ")
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-above-tile matrix
                                                                           pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-prev-tile  matrix
                                                                           pos))   ?t))
                                   "╯")
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-prev-above-tile matrix
                                                                                pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-above-tile      matrix
                                                                                pos))   ?t))
                                   "╮")
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-prev-above-tile matrix
                                                                                pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-prev-tile       matrix
                                                                                pos))   ?t))
                                   "╰")
                             ((eq (klere-fos--matrix-tile-type
                                    (klere-fos--matrix-get-prev-above-tile matrix
                                                                           pos))   ?t)
                                   "┼")
                             ((eq (klere-fos--matrix-tile-type
                                    (klere-fos--matrix-get-above-tile matrix
                                                                      pos))   ?t)
                                   "│")
                             ((eq (klere-fos--matrix-tile-type
                                    (klere-fos--matrix-get-prev-tile matrix
                                                                     pos))   ?t)
                                   "─")
                             (t    "╭")))
                         ('(top right)
                            (cond
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-next-above-tile matrix
                                                                                pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-above-tile      matrix
                                                                                pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-next-tile       matrix
                                                                                pos))   ?t))
                                   " ")
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-above-tile matrix
                                                                           pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-next-tile  matrix
                                                                           pos))   ?t))
                                   "╰")
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-next-above-tile matrix
                                                                                pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-above-tile      matrix
                                                                                pos))   ?t))
                                   "╭")
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-next-above-tile matrix
                                                                                pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-next-tile       matrix
                                                                                pos))   ?t))
                                   "╯")
                             ((eq (klere-fos--matrix-tile-type
                                    (klere-fos--matrix-get-next-above-tile matrix
                                                                           pos))    ?t)
                                   "┼")
                             ((eq (klere-fos--matrix-tile-type
                                    (klere-fos--matrix-get-above-tile matrix
                                                                      pos))    ?t)
                                   "│")
                             ((eq (klere-fos--matrix-tile-type
                                    (klere-fos--matrix-get-next-tile matrix
                                                                     pos))   ?t)
                                   "─")
                             (t    "╮")))
                         ('(left bottom)
                            (cond
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-prev-below-tile matrix
                                                                                pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-below-tile      matrix
                                                                                pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-prev-tile       matrix
                                                                                pos))   ?t))
                                   " ")
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-below-tile      matrix
                                                                                pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-prev-tile       matrix
                                                                                pos))   ?t))
                                   "╮")
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-prev-below-tile matrix
                                                                                pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-below-tile      matrix
                                                                                pos))   ?t))
                                   "╯")
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-prev-below-tile matrix
                                                                                pos))   ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-prev-tile       matrix
                                                                                pos))   ?t))
                                   "╭")
                             ((eq (klere-fos--matrix-tile-type
                                    (klere-fos--matrix-get-prev-below-tile matrix
                                                                           pos))    ?t)
                                   "┼")
                             ((eq (klere-fos--matrix-tile-type
                                    (klere-fos--matrix-get-below-tile matrix
                                                                      pos))    ?t)
                                   "│")
                             ((eq (klere-fos--matrix-tile-type
                                    (klere-fos--matrix-get-prev-tile matrix
                                                                     pos))    ?t)
                                   "─")
                             (t    "╰")))
                         ('(right bottom)
                            (cond
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-next-below-tile matrix
                                                                                pos))    ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-below-tile      matrix
                                                                                pos))    ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-next-tile       matrix
                                                                                pos))    ?t))
                                   " ")
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-below-tile      matrix
                                                                                pos))    ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-next-tile       matrix
                                                                                pos))    ?t))
                                   "╭")
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-next-below-tile matrix
                                                                                pos))    ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-below-tile      matrix
                                                                                pos))    ?t))
                                   "╰")
                             ((and (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-next-below-tile matrix
                                                                                pos))    ?t)
                                   (eq (klere-fos--matrix-tile-type
                                         (klere-fos--matrix-get-next-tile       matrix
                                                                                pos))    ?t))
                                   "╮")
                             ((eq (klere-fos--matrix-tile-type
                                    (klere-fos--matrix-get-next-below-tile matrix
                                                                           pos))    ?t)
                                   "┼")
                             ((eq (klere-fos--matrix-tile-type
                                    (klere-fos--matrix-get-below-tile matrix
                                                                      pos))    ?t)
                                   "│")
                             ((eq (klere-fos--matrix-tile-type
                                    (klere-fos--matrix-get-next-tile matrix
                                                                     pos))    ?t)
                                   "─")
                             (t    "╯")))))))
         ( sidesfn (lambda (placement)
                     (let* ((meta (klere-fos--mu-metadata-get
                                    (klere-fos--matrix-get matrix pos)))
                            (type (klere-fos--mu-metadata-get-type meta))
                            (pos  (and (eq type 'Bookcase)
                                       (klere-fos--mu-metadata-bookcase-position-get meta))))
                       (pcase placement
                         ('(top)          (if (eq pos 'top)   char "═"))
                         ('(left)         (if (eq pos 'three) char "║"))
                         ('(right)        (if (eq pos 'three) char "║"))
                         ('(bottom)       "═")
                         ('(top left)     (if (eq pos 'top)
                                              "╣"
                                            (if (eq pos 'three) "╩" "╬")))
                         ('(top right)    (if (eq pos 'top)
                                              "╠"
                                            (if (eq pos 'three) "╩" "╬")))
                         ('(left  bottom) (pcase type
                                            ('Exit     "╩")
                                            ('Bookcase (if (eq pos 'three) "╦" "╬"))
                                            (_         "╬")))
                         ('(right bottom) (pcase type
                                            ('Exit     "╩")
                                            ('Bookcase (if (eq pos 'three) "╦" "╬"))
                                            (_         "╬"))))))))
    (concat (funcall borderfn '(top left)) (funcall borderfn '(top)) (funcall borderfn '(top middle))
                                           (funcall borderfn '(top)) (funcall borderfn '(top right))
            "\n"
            (funcall borderfn '(left)) (funcall sidesfn '(top left))
                                       (funcall sidesfn '(top))
                                       (funcall sidesfn '(top right)) (funcall borderfn '(right))
            "\n"
            (funcall borderfn '(left middle)) (funcall sidesfn '(left))
                                              (klere-fos--generate-walkable-tile-icon matrix pos)
                                              (funcall sidesfn '(right)) (funcall borderfn '(right middle))
            "\n"
            (funcall borderfn '(left)) (funcall sidesfn '(left  bottom))
                                       (funcall sidesfn '(bottom))
                                       (funcall sidesfn '(right bottom)) (funcall borderfn '(right))
            "\n"
            (funcall borderfn '(left bottom)) (funcall borderfn '(bottom)) (funcall borderfn '(bottom middle))
                                              (funcall borderfn '(bottom)) (funcall borderfn '(right bottom)))))

(defun klere-fos--generate-tile (matrix pos &optional tile)
  ""

  (let ((ti (or tile (klere-fos--matrix-get matrix pos))))
    (pcase (klere-fos--matrix-tile-type ti)
      (?N (klere-fos--generate-navigate-tile    matrix pos))
      (?B (klere-fos--generate-castle-wall-tile matrix pos))
      (?W (klere-fos--generate-river-tile       matrix pos))
      (?F (klere-fos--generate-forest-tile      matrix pos))
      (?M (klere-fos--generate-mountain-tile    matrix pos))
      (?t (klere-fos--generate-house-floor-tile matrix pos))
      (?E                             "     \n     \n     "))))



(defun klere-fos--maps-insert (str-or-matr)
  ""

  (let* ((matrix (if (stringp str-or-matr)
                     (klere-fos--matrix-from-string str-or-matr)
                   str-or-matr))
         (row    (make-string (1+ (* (length (elt matrix 0)) 4)) ?\ ))
         (init   (apply #'concat
                        (mapcar (lambda (_)
                                  (concat row "\n"))
                                (make-list (1+ (* 4 (length matrix))) "p")))))
    (erase-buffer)
    ;; I'm not sure if we can insert where the cursor cannot navigate
    ;; so let's just insert spaces that we'll overwrite, later
    (insert init)

    (let ((p (lambda (tile x y)
               (let ((index 0))
                 (mapcar
                   (lambda (row)
                     (goto-line      (+ (klere-fos--maps-pos-to-line y) index))
                     (move-to-column    (klere-fos--maps-pos-to-col  x))

                     (delete-region (point) (+ (point) 5))

                     (insert row)

                     (setq index (1+ index)))
                   (split-string (klere-fos--generate-tile matrix
                                                           (cons x y)
                                                           tile)       "\n"))))))
      (klere-fos--matrix-cycle-with-index matrix p '(?N))
      (klere-fos--matrix-cycle-with-index matrix p '(?t))
      (klere-fos--matrix-cycle-with-index matrix p '(?F ?M))
      (klere-fos--matrix-cycle-with-index matrix p '(?B))
      (klere-fos--matrix-cycle-with-index matrix p '(?W)))))

(defun klere-fos--maps-tile-icon-update (matrix pos)
  ""

  (read-only-mode 0)
  (save-excursion
    ;; goto Center of map tile
    (goto-line      (1+ (1+ (klere-fos--maps-pos-to-line (cdr pos)))))
    (move-to-column (1+ (1+ (klere-fos--maps-pos-to-col  (car pos)))))

    (delete-char 1)
    (insert (klere-fos--generate-walkable-tile-icon matrix pos)))
  (read-only-mode 1))

(defun klere-fos--maps-position-insert (matrix pos &optional prev-data)
  ""

  (let ((replace-chars (lambda (chars x y &optional save-chars)
                         (goto-line      (klere-fos--maps-pos-to-line y))
                         (move-to-column (klere-fos--maps-pos-to-col  x))

                         (let ((return-point (point))
                               (save         '()))
                           (when save-chars
                             (push (buffer-substring (point) (+ (point) 5))
                                   save))
                           (delete-region (point) (+ (point) 5))
                           (insert (cl-first chars))

                           (dotimes (count 3)
                             (goto-line      (+ (klere-fos--maps-pos-to-line y)
                                                (1+ count)))
                             (move-to-column    (klere-fos--maps-pos-to-col  x))

                             (when save-chars
                               (push (buffer-substring (point) (+ (point) 1))
                                     save))
                             (delete-region (point) (+ (point) 1))
                             (insert (elt chars (1+ (* count 2))))

                             (forward-char 3)

                             (when save-chars
                               (push (buffer-substring (point) (+ (point) 1))
                                     save))
                             (delete-region (point) (+ (point) 1))
                             (insert (elt chars (1+ (1+ (* count 2))))))

                           (goto-line      (+ (klere-fos--maps-pos-to-line y) 4))
                           (move-to-column    (klere-fos--maps-pos-to-col  x))

                           (when save-chars
                             (push (buffer-substring (point) (+ (point) 5))
                                   save))
                           (delete-region (point) (+ (point) 5))
                           (insert (car (last chars)))

                           ;; (goto-char return-point)
                           (backward-char 1)

                           (cons (car save-chars) (nreverse save))))))
    (when prev-data
      (funcall replace-chars (cdr prev-data) (caar prev-data) (cdar prev-data)))

    (funcall replace-chars (let* ((t1 (klere-fos--matrix-get matrix pos))
                                  (c  (lambda (t2)
                                        (and (eq (klere-fos--matrix-tile-type t1) ?N)
                                             (eq (klere-fos--matrix-tile-type t2) ?N)))))
                             (list (concat "┏━"
                                           (if (funcall c (klere-fos--matrix-get-above-tile matrix
                                                                                            pos))
                                               "│"
                                             "━")
                                           "━┓")
                                   "┃" "┃"
                                   (if (funcall c (klere-fos--matrix-get-prev-tile matrix
                                                                                   pos))
                                       "─"
                                     "┃")
                                   (if (funcall c (klere-fos--matrix-get-next-tile matrix
                                                                                   pos))
                                       "─"
                                     "┃")
                                   "┃" "┃"
                                   (concat "┗━"
                                           (if (funcall c (klere-fos--matrix-get-below-tile matrix
                                                                                            pos))
                                               "│"
                                             "━")
                                           "━┛")))
                           (car pos)
                           (cdr pos)
                           (cons pos '()))))



(defun klere-fos--maps-move-cursor-position (matrix map-pos-data funct)
  ""

  (klere-fos--maps-position-insert matrix
                                   (funcall funct matrix (car map-pos-data))
                                   map-pos-data))

(defun klere-fos--maps-move-cursor-position-up (matrix map-pos-data)
  ""

  (klere-fos--maps-move-cursor-position matrix
                                        map-pos-data
                                        #'klere-fos--matrix-get-above-coordinate))

(defun klere-fos--maps-move-cursor-position-down (matrix map-pos-data)
  ""

  (klere-fos--maps-move-cursor-position matrix
                                        map-pos-data
                                        #'klere-fos--matrix-get-below-coordinate))

(defun klere-fos--maps-move-cursor-position-left (matrix map-pos-data)
  ""

  (klere-fos--maps-move-cursor-position matrix
                                        map-pos-data
                                        #'klere-fos--matrix-get-prev-coordinate))

(defun klere-fos--maps-move-cursor-position-right (matrix map-pos-data)
  ""

  (klere-fos--maps-move-cursor-position matrix
                                        map-pos-data
                                        #'klere-fos--matrix-get-next-coordinate))



(defun klere-fos--maps-move-cursor-position-to-row-pos (matrix
                                                        map-pos-data
                                                         tile-fn
                                                        coord-fn     &optional tile-types)
  ""

  (klere-fos--maps-position-insert matrix
                                   (if (not tile-types)
                                       (cons (length (car matrix))
                                             (cdar map-pos-data))
                                     (let ((unfinished-p t)
                                           (pos          (car map-pos-data)))
                                       (while unfinished-p
                                         (if (not (member (klere-fos--matrix-tile-type
                                                            (funcall tile-fn matrix pos))
                                                          tile-types))
                                             (setq unfinished-p nil)
                                           (setq pos (funcall coord-fn matrix
                                                                       pos))))

                                       pos))
                                   map-pos-data))

(defun klere-fos--maps-move-cursor-position-to-row-beg (matrix
                                                        map-pos-data
                                                        &optional tile-types)
  ""

  (klere-fos--maps-move-cursor-position-to-row-pos matrix
                                                   map-pos-data
                                                   #'klere-fos--matrix-get-prev-tile
                                                   #'klere-fos--matrix-get-prev-coordinate
                                                   tile-types))

(defun klere-fos--maps-move-cursor-position-to-row-end (matrix
                                                        map-pos-data
                                                        &optional tile-types)
  ""

  (klere-fos--maps-move-cursor-position-to-row-pos matrix
                                                   map-pos-data
                                                   #'klere-fos--matrix-get-next-tile
                                                   #'klere-fos--matrix-get-next-coordinate
                                                   tile-types))

(defun klere-fos--maps-move-cursor-position-to-row-top (matrix
                                                        map-pos-data
                                                        &optional tile-types)
  ""

  (klere-fos--maps-move-cursor-position-to-row-pos matrix
                                                   map-pos-data
                                                   #'klere-fos--matrix-get-above-tile
                                                   #'klere-fos--matrix-get-above-coordinate
                                                   tile-types))

(defun klere-fos--maps-move-cursor-position-to-row-bot (matrix
                                                        map-pos-data
                                                        &optional tile-types)
  ""

  (klere-fos--maps-move-cursor-position-to-row-pos matrix
                                                   map-pos-data
                                                   #'klere-fos--matrix-get-below-tile
                                                   #'klere-fos--matrix-get-below-coordinate
                                                   tile-types))



(defun klere-fos--maps-center ()
  ""
  (interactive)

  (recenter)

  (let ((mid      (/ (window-width) 2))
        (line-len (save-excursion (end-of-line) (current-column)))
        (cur      (current-column)))
    (when (< mid cur)
      (set-window-hscroll (selected-window)
                          (- cur mid)))))



(provide 'klere-fos--maps)

;;; klere-fos--maps.el ends here
