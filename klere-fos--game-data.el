;;; klere-fos--game-data.el --- Klere Fòs: Game Data                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Klere-F-s
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0
;; Keywords: game, rpg

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(require 'klere-fos--items)
(require 'klere-fos--characters)
;; setf
(require 'gv)

(defcustom klere-fos----game-data-path (expand-file-name "~/.kf/")
  "The path where Klere Fòs game data will be saved.

If the directory does not exist, Klere Fòs will create it.

Make sure to end your path with a slash so that all directories in the path can
be easily recognized."
  :type  'string
  :group 'klere-fos--game-data)

(defvar klere-fos----game-data '()
  "Current save data for the running session of Klere Fòs.")

(defun klere-fos--game-data-new-game (name)
  ""

  (make-directory klere-fos----game-data-path t)

  (let ((current-time (time-convert nil 'integer)))
    (setq klere-fos----game-data
          `((:run-time        .                 0)
            (:last-play-start .     ,current-time)
            (:user-name       .             ,name)
            (:current-team    . ((:main . ,(funcall klere-fos--characters-main
                                                    name))))
            (:save-place      . klere-fos--level1)
            (:save-file       . ,(concat (with-temp-buffer
                                           (let ((buffer-file-name name))
                                             (car (last
                                                    (split-string
                                                      (make-auto-save-file-name)
                                                      "/")))))
                                         (number-to-string current-time)
                                         ".el")))))

  (klere-fos--game-data-save))

(defmacro klere-fos--game-data-defun-datum (get-or-set datum-name datum-desc-lbl)
  ""

  (let ((str (symbol-name datum-name)))
    `(defun ,(intern (concat "klere-fos--game-data-"
                             (symbol-name get-or-set)
                             "-"
                             str))
            ,(if (eq get-or-set 'get) '() '(value))
       ,(concat (if (eq get-or-set 'get) "Get" "Set") " the " datum-desc-lbl
                " from the current game data, via the `klere-fos----game-data' "
                "variable.")

       ,(if (eq get-or-set 'get)
            `(alist-get ,(intern (concat ":" (symbol-name datum-name)))
                        klere-fos----game-data)
          `(setf (alist-get ,(intern (concat ":" (symbol-name datum-name)))
                            klere-fos----game-data)
                 value)))))

(klere-fos--game-data-defun-datum get run-time "total running time of the game")
(klere-fos--game-data-defun-datum set run-time "total running time of the game")

(klere-fos--game-data-defun-datum get
                                  last-play-start
                                  "last date/time the player loaded their last save state")
(klere-fos--game-data-defun-datum set
                                  last-play-start
                                  "last date/time the player loaded their last save state")

(klere-fos--game-data-defun-datum get user-name "total running time of the game")
(klere-fos--game-data-defun-datum set user-name "total running time of the game")

(klere-fos--game-data-defun-datum get
                                  current-team
                                  "curret team – all characters – the player has found throughout zir gameplay")
(defun klere-fos--game-data-get-team-char (char-key)
  ""

  (alist-get char-key (klere-fos--game-data-get-current-team)))
(defun klere-fos--game-data-update-team-char (char-key char)
  ""

  (setcdr (assq char-key (klere-fos--game-data-get-current-team)) char))

(klere-fos--game-data-defun-datum get save-place "symbol of the last function a player saved at")
(klere-fos--game-data-defun-datum set save-place "symbol of the last function a player saved at")

(klere-fos--game-data-defun-datum get save-file "file name of where save data is stored")
(klere-fos--game-data-defun-datum set save-file "file name of where save data is stored")

(defun klere-fos--game-data-save ()
  ""

  (with-temp-buffer
    (setcdr (assq :run-time klere-fos----game-data)
            (+ (klere-fos--game-data-get-run-time)
               (- (time-convert nil 'integer)
                  (klere-fos--game-data-get-last-play-start))))

    (with-temp-file (concat klere-fos----game-data-path
                            (klere-fos--game-data-get-save-file))
      (insert (format "%S" klere-fos----game-data)))))

(defun klere-fos--game-data-fetch (path)
  ""

  (with-temp-buffer
    (insert-file-contents path)

    (eval (car (read-from-string (format "(progn '%s)"
                                         (buffer-substring-no-properties (point-min)
                                                                         (point-max))))))))

(defun klere-fos--game-data-load (data)
  ""

  (setq klere-fos----game-data data)

  (setcdr (assq :last-play-start klere-fos----game-data)
          (time-convert nil 'integer))

  klere-fos----game-data)



(provide 'klere-fos--game-data)

;;; klere-fos--game-data.el ends here
