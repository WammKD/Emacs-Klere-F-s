;;; klere-fos--utils.el --- Klere Fòs: Utilities                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Klere-F-s
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0
;; Keywords: game, rpg

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(defmacro or-eq (compare-against &rest elems)
  "Return true if anything in ELEMS is `eq' to COMPARE-AGAINST.

Alternatively, COMPARE-AGAINST can be a function which takes one argument and
all of ELEMS will be submitted, one by one; if any result in the function
returning a true value, that value is returned."

  (let ((eqs (mapcar (lambda (elem)
                       (if (functionp compare-against)
                           `(funcall #',compare-against ,elem)
                         `(eq ,compare-against ,elem)))          elems)))
    `(or ,@eqs)))

(defun alist-replace (key value alist)
  "A nondestructive way to replace values in an alist.

The value associated with KEY in ALIST gets replaced with VALUE in a new alist,
performing no modifications to ALIST."

  (cons `(,key . ,value) (remove (funcall (if (symbolp key) 'assq 'assoc)
                                          key
                                          alist)                           alist)))

(defmacro klere-fos--with-curr-buff-lambda (buffer-variable-name &rest body)
  ""

  (declare (indent 1))

  `(lambda (,buffer-variable-name)
     (with-current-buffer ,buffer-variable-name
       ,@body)))



(defvar klere-fos--restricted-mode-map (let ((map (make-sparse-keymap)))
                                         (suppress-keymap map t)

                                         map)
  "Keymap for `klere-fos--restricted-mode-mode'.")

(define-derived-mode klere-fos--restricted-mode fundamental-mode "KlereFòs:Restricted"
  "Major mode for disallowing self-insert in Klere Fòs.")



(provide 'klere-fos--utils)

;;; klere-fos--utils.el ends here
