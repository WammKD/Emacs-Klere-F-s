;;; klere-fos--items.el --- Klere Fòs: Items                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Klere-F-s
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0
;; Keywords: game, rpg

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(require 'klere-fos--characters)

;;;;;;;;;;;;;;;;;;;;;;;
;;                   ;;
;;;  W E A P O N S  ;;;
;;                   ;;
;;;;;;;;;;;;;;;;;;;;;;;
(defun klere-fos--items-create-weapon (name description attack cost)
  ""

  `((:name        .        ,name)
    (:description . ,description)
    (:attack      .      ,attack)
    (:cost        .        ,cost)))

;;; Swords
(defvar klere-fos----items-wooden-sword
        (klere-fos--items-create-weapon "Wooden Sword"
                                        (concat "A short sword made of wood. A "
                                                "beginner's weapon to practice "
                                                "with before moving on to more "
                                                "damaging objects.")
                                        3
                                        60)
  "")
(defvar klere-fos----items-short-sword
        (klere-fos--items-create-weapon "Short Sword"
                                        ""
                                        5
                                        100)
  "")
(defvar klere-fos----items-middle-sword
        (klere-fos--items-create-weapon "Middle Sword"
                                        ""
                                        8
                                        250)
  "")
(defvar klere-fos----items-long-sword
        (klere-fos--items-create-weapon "Long Sword"
                                        ""
                                        12
                                        620)
  "")
(defvar klere-fos----items-steel-sword
        (klere-fos--items-create-weapon "Steel Sword"
                                        ""
                                        18
                                        1120)
  "")

;;; Axes
(defvar klere-fos----items-short-ax
        (klere-fos--items-create-weapon "Short Ax"
                                        ""
                                        5
                                        100)
  "")
(defvar klere-fos----items-hand-ax
        (klere-fos--items-create-weapon "Hand Ax"
                                        ""
                                        9
                                        250)
  "")
(defvar klere-fos----items-middle-ax
        (klere-fos--items-create-weapon "Middle Ax"
                                        ""
                                        13
                                        610)
  "")
(defvar klere-fos----items-large-ax
        (klere-fos--items-create-weapon "Large Ax"
                                        ""
                                        17
                                        1120)
  "")

;;; Arrows
(defvar klere-fos----items-wooden-arrow
        (klere-fos--items-create-weapon "Wooden Arrow"
                                        ""
                                        3
                                        60)
  "")
(defvar klere-fos----items-iron-arrow
        (klere-fos--items-create-weapon "Iron Arrow"
                                        ""
                                        5
                                        100)
  "")
(defvar klere-fos----items-steel-arrow
        (klere-fos--items-create-weapon "Steel Arrow"
                                        ""
                                        7
                                        200)
  "")
(defvar klere-fos----items-elven-arrow
        (klere-fos--items-create-weapon "Elven Arrow"
                                        ""
                                        11
                                        500)
  "")



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                    ;;
;;;  S T A T U S  A F F E C T O R S  ;;;
;;                                    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun klere-fos--items-create-status-affecter (name     description
                                                modifier cost        resale)
  ""

  `((:name         .        ,name)
    (:description  . ,description)
    (:modifier     .    ,modifier)
    (:cost         .        ,cost)
    (:resale-price .      ,resale)))

(defmacro klere-fos--items-get-status-affecter-data (data-key desc-lbl)
  ""

  (let ((str (symbol-name data-key)))
    `(defun ,(intern (concat "klere-fos--items-get-" str)) (item)
       ,(concat "Fetch the  " desc-lbl " of the ITEM.")

       (alist-get ,(intern (concat ":" str)) item))))

(klere-fos--items-get-status-affecter-data name "Name")
(klere-fos--items-get-status-affecter-data description  "Name")
(klere-fos--items-get-status-affecter-data modifier     "Function that Modifies the Character")
(klere-fos--items-get-status-affecter-data cost         "Amount the Item Costs")
(klere-fos--items-get-status-affecter-data resale-price "Price the Item, if Resold,")

(defvar klere-fos----items-healing-herb
        (klere-fos--items-create-status-affecter
          "Healing Herb"
          (concat "A tasteless herb with healing properties. "
                  "Can heal 10 hit points (HP).")
          (lambda (char)
            (let ((max (klere-fos--characters-get-base-hp char))
                  (new (+ (klere-fos--characters-get-current-hp char) 10)))
              (alist-replace :current-stats
                             (alist-replace :hp
                                            (if (< new max) new max)
                                            (alist-get :current-stats char))
                             char)))
          10
          10)
  "")

(defvar klere-fos----items-bezoar
        (klere-fos--items-create-status-affecter
          "Bezoar"
          (concat "A strange stone said to cure poison; best not to ask too "
                  "much about where it came from. 'Has a 25% success rate.")
          (lambda (char)
            (if (= (random 4) 0)
                (alist-replace :status (remq 'poison (alist-get :status char)))
              char))
          5
          1)
  "")



(provide 'klere-fos--items)

;;; klere-fos--items.el ends here
