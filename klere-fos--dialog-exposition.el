;;; klere-fos--dialog-exposition.el --- Klere Fòs: Battle Maps                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Klere-F-s
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0
;; Keywords: game, rpg

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(require 'klere-fos--utils)
;; string-join
(require 'subr-x)
;; run-at-time
(require 'timer)

(defcustom klere-fos---dia-exp-print-interval 0.25
  "The interval, in seconds, that dialogue/exposition should print to the screen.

Shoter times will mean that letters show up to the screen more quickly while
longer times will print each letter to the buffer more slowly, with a longer
period of time between when the next letter will show."
  :type  'number
  :group 'klere-fos--dia-exp)

(defcustom klere-fos---dia-exp-message-interval 0.05
  "The interval, in seconds, that dialogue should `message' to the screen.

Shoter times will mean that letters show up in the minibuffer more quickly while
longer times will print each letter to the minibuffer more slowly, with a longer
period of time between when the next letter will show."
  :type  'number
  :group 'klere-fos--dia-exp)



(defun klere-fos-dia-exp-loading-finish ()
  ""
  (interactive)

  (setq klere-fos----dia-exp-skipped t))

(defvar klere-fos--dia-exp-loading-mode-map (let ((mode-map klere-fos--restricted-mode-map))
                                              (define-key mode-map (kbd "SPC") #'klere-fos-dia-exp-loading-finish)

                                              mode-map)
  "Keymap for `klere-fos--dia-exp-loading-mode-mode'.")

(define-derived-mode klere-fos--dia-exp-loading-mode fundamental-mode "KlereFòs:Dialogue-Exposition-Loading"
  "Major mode for disallowing self-insert while dialogue screens are loading.")



(defvar klere-fos--dia-exp-mode-map (let ((mode-map (make-sparse-keymap)))
                                      (define-key mode-map (kbd "SPC") #'klere-fos-dia-exp-next)

                                      mode-map)
  "Keymap for `klere-fos--dia-exp-mode-mode'.")

(define-derived-mode klere-fos--dia-exp-mode fundamental-mode "KlereFòs:Dialogue-Exposition"
  "Major mode for talking to characters or witnessing scenes in Klere Fòs."

  (read-only-mode 1))



(defun klere-fos-dia-exp-question-hint ()
  ""
  (interactive)

  (message "Hint: respond with the letter (e.g. A, B, etc.) of the answer you want to use"))

(defvar klere-fos--dia-exp-question-mode-map (let ((mode-map (make-sparse-keymap)))
                                               (define-key mode-map (kbd "SPC") #'klere-fos-dia-exp-question-hint)

                                               mode-map)
  "Keymap for `klere-fos--dia-exp-question-mode-mode'.")

(define-derived-mode klere-fos--dia-exp-question-mode fundamental-mode "KlereFòs:Dialogue-Exposition-Question"
  "Major mode for posing questions to the player in Klere Fòs."

  (read-only-mode 1))



(defun klere-fos--dia-exp-message (remaining &optional old-msg)
  ""

  (if (stringp remaining)
      (klere-fos--dia-exp-message (string-to-list remaining))
    (when-let* ((char                           (car remaining))
                (new-msg (concat old-msg (char-to-string char))))
      (message new-msg)

      (run-at-time klere-fos---dia-exp-message-interval
                   nil
                   (lambda ()
                     (klere-fos--dia-exp-message (cdr remaining) new-msg))))))

(defun klere-fos--dia-exp-insert (buffer               indent       text
                                  line-limit &optional current-word run-fast-p)
  ""

  (let ((process (lambda ()
                   (let* ((possible-next-word (substring current-word 1))
                          (current-word-done  (string-empty-p possible-next-word))
                          (next-word          (if current-word-done
                                                  (car text)
                                                possible-next-word)))
                     (with-current-buffer buffer
                       (when current-word-done
                         (end-of-buffer)

                         (if (> (+ (current-column) (length next-word))
                                line-limit)
                             (insert "\n" indent)
                           (insert " "))))

                     (klere-fos--dia-exp-insert buffer                 indent
                                                (if current-word-done
                                                    (cdr text)
                                                  text)                line-limit
                                                next-word              run-fast-p)))))
    (with-current-buffer buffer
      (if (stringp text)
          ;; Starting point
          (let ((lst (split-string text " ")))
            (end-of-buffer)
            (insert indent)

            (klere-fos--dia-exp-insert buffer     indent    (cdr lst)
                                       line-limit (car lst) run-fast-p))
        (if (not (or current-word text))
            (klere-fos--dia-exp-mode)
          (end-of-buffer)
          (insert (substring current-word 0 1))

          (run-at-time (if klere-fos----dia-exp-skipped
                           0
                         (if run-fast-p 0.05 klere-fos---dia-exp-print-interval))
                       nil
                       process))))))

;; To be safe, limit `speech-or-answers' to 280 characters
(defun klere-fos--dia-exp-render (speech-or-answers graphic &optional then fast-p)
  ""

  (klere-fos--dia-exp-loading-mode)
  (setq klere-fos----dia-exp-skipped nil)
  (if then
      (defun klere-fos-dia-exp-next ()
        ""
        (interactive)

        (funcall then (current-buffer)))
    (fmakunbound 'klere-fos-dia-exp-next))

  (read-only-mode 0)
  (erase-buffer)



  (let* ((win-width   (1- (window-width)))
         (win-height  (1- (window-height)))
         ;; (divider     (concat "+" (make-string (- win-width 2) ?\—) "+"))
         ;; (divider     (concat "╬" (make-string (- win-width 2) ?═) "╬"))
         ;; (divider     (concat "╪" (make-string (- win-width 2) ?═) "╪"))
         (top-divider (concat "╒" (make-string (- win-width 2) ?═) "╕"))
         (bot-divider (concat "╞" (make-string (- win-width 2) ?═) "╡"))
         (indent      (if (< win-width 87) "  " "    "))
         (row         (make-string win-width ?\ ))
         (init        (apply #'concat (mapcar (lambda (_)
                                                (concat row "\n"))
                                              (make-list (- win-height 8) "p")))))
    (insert top-divider "\n")

    (insert init)

    (insert bot-divider "\n\n")

    (if (stringp speech-or-answers)
        (klere-fos--dia-exp-insert (current-buffer)  indent
                                   speech-or-answers (- win-width (length indent))
                                   then              fast-p)
      (mapcar (lambda (choice)
                (insert indent choice "\n")) speech-or-answers)

      (goto-line (- win-height (length speech-or-answers)))
      (forward-char (length indent))

      (klere-fos--dia-exp-question-mode))))



(provide 'klere-fos--dialog-exposition)

;;; klere-fos--dialog-exposition.el ends here
