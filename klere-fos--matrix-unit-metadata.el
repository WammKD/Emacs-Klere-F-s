;;; klere-fos--matrix-unit-metadata.el --- Klere Fòs: Matrix Unit Metadata                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Klere-F-s
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0
;; Keywords: game, rpg

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(require 'klere-fos--matrices)
(require 'klere-fos--dialog-exposition)
(require 'klere-fos--items)
(require 'klere-fos--game-data)
;; if-let*
(require 'subr-x)

(defun klere-fos--mu-metadata-get (unit)
  ""

  (when (consp unit)
    (cdr unit)))
(defun klere-fos--mu-metadata-create (type seen revealed activity)
  ""

  `((:type     . ,type)
    (:seen     . ,seen)
    (:revealed . ,revealed)
    (:activity . ,activity)))

(defun klere-fos--mu-metadata-get-type (metadata)
  ""

  (alist-get :type (alist-get :type metadata)))

(defun klere-fos--mu-metadata-seen-p (metadata)
  ""

  (alist-get :seen metadata))
(defun klere-fos--mu-metadata-set-seen (metadata value)
  ""

  (setcdr (assq :seen metadata) value))

(defun klere-fos--mu-metadata-revealed-p (metadata)
  ""

  (alist-get :revealed metadata))
(defun klere-fos--mu-metadata-set-revealed (metadata value)
  ""

  (setcdr (assq :revealed metadata) value))

(defun klere-fos--mu-metadata-activate-next (metadata matrix pos &optional rerun-p)
  ""

  (funcall (alist-get :activity metadata) matrix pos rerun-p))



(defun klere-fos--mu-metadata-create-person (&optional name)
  ""

  `((:type . Person)
    (:name .  ,name)))

(defmacro klere-fos--mu-metadata-person-lambda (matrix pos rerun-p metadata message &rest body)
  ""

  (declare (indent 3))

  `(lambda (,matrix ,pos &optional ,rerun-p)
     (if-let* ((,metadata (klere-fos--mu-metadata-get (klere-fos--matrix-get ,matrix
                                                                             ,pos)))
               (seen-p    (and (not ,rerun-p)
                               (klere-fos--mu-metadata-seen-p ,metadata))))
         (klere-fos--dia-exp-message ,message)
       (klere-fos--mu-metadata-set-seen     ,metadata t)
       (klere-fos--mu-metadata-set-revealed ,metadata t)

       ,@body)))



(defun klere-fos--mu-metadata-create-house (&optional locked-p name)
  ""

  `((:type   .     House)
    (:locked . ,locked-p)
    (:name   .     ,name)
    (:map    .      ,nil)))

(defmacro klere-fos--mu-metadata-house-lambda (matrix pos metadata &rest body)
  ""

  (declare (indent 3))

  `(lambda (,matrix ,pos &optional rerun-p)
     (let ((,metadata (klere-fos--mu-metadata-get (klere-fos--matrix-get ,matrix
                                                                         ,pos))))
       (klere-fos--mu-metadata-set-seen     ,metadata t)
       (klere-fos--mu-metadata-set-revealed ,metadata t)

       (if (klere-fos--mu-metadata-house-locked-p ,metadata)
           (progn
             (klere-fos--maps-tile-icon-update ,matrix ,pos)

             (klere-fos--dia-exp-message (concat "No one must be home; "
                                                 "the house is locked.")))
         ,@body))))

(defun klere-fos--mu-metadata-house-locked-p (metadata)
  ""

  (alist-get :locked (alist-get :type metadata)))

(defun klere-fos--mu-metadata-house-get-map (metadata)
  ""

  (alist-get :map (alist-get :type metadata)))
(defun klere-fos--mu-metadata-house-set-map (metadata value)
  ""

  (setcdr (assq :map (alist-get :type metadata)) value))



(defun klere-fos--mu-metadata-create-chest (items &optional locked-p)
  ""

  `((:type   .     Chest)
    (:locked . ,locked-p)
    (:items  .    ,items)))

(defmacro klere-fos--mu-metadata-chest-lambda (matrix pos metadata &rest body)
  ""

  (declare (indent 3))

  `(lambda (,matrix ,pos &optional rerun-p)
     (let ((,metadata (klere-fos--mu-metadata-get (klere-fos--matrix-get ,matrix
                                                                         ,pos))))
       (klere-fos--mu-metadata-set-seen     ,metadata t)
       (klere-fos--mu-metadata-set-revealed ,metadata t)

       (klere-fos--maps-tile-icon-update ,matrix ,pos)

       (if (klere-fos--mu-metadata-chest-locked-p ,metadata)
           (klere-fos--dia-exp-message (concat "This chest is locked! Maybe "
                                               "there's a key for it nearby…"))
         (let ((items (klere-fos--mu-metadata-chest-get-items ,metadata)))
           (if (not items)
               (klere-fos--dia-exp-message "The chest is empty!")
             (let* ((team   (mapcar (lambda (key-and-char)
                                      (cons (klere-fos--characters-get-name
                                              (cdr key-and-char))
                                            key-and-char))
                                    (klere-fos--game-data-get-current-team)))
                    (result (alist-get
                              (ido-completing-read
                                (concat "The chest contains "
                                        (mapconcat (lambda (item)
                                                     (klere-fos--items-get-name item))
                                                   items
                                                   ", ")
                                        "! Give to whom? ")
                                team)
                              team)))
               (klere-fos--game-data-update-team-char
                 (car result)
                 (klere-fos--characters-add-items-to-held
                   (cdr result)
                   (klere-fos--mu-metadata-chest-get-items ,metadata))))

             (klere-fos--mu-metadata-chest-set-items ,metadata '())))

         ,@body))))

(defun klere-fos--mu-metadata-chest-locked-p (metadata)
  ""

  (alist-get :locked (alist-get :type metadata)))

(defun klere-fos--mu-metadata-chest-get-items (metadata)
  ""

  (alist-get :items (alist-get :type metadata)))
(defun klere-fos--mu-metadata-chest-set-items (metadata value)
  ""

  (setcdr (assq :items (alist-get :type metadata)) value))



(defun klere-fos--mu-metadata-create-exit ()
  ""

  `((:type . Exit)
    (:map  . ,nil)))



(defun klere-fos--mu-metadata-create-bookcase (message position)
  ""

  `((:type     . Bookcase)
    (:message  . ,message)
    (:position . ,position)))

(defun klere-fos--mu-metadata-bookcase-lambda ()
  ""

  (lambda (matrix pos &optional rerun-p)
    (let ((metadata (klere-fos--mu-metadata-get (klere-fos--matrix-get matrix
                                                                       pos))))
      (klere-fos--mu-metadata-set-seen metadata t)

      (klere-fos--dia-exp-message
        (klere-fos--mu-metadata-bookcase-message-get metadata)))))

(defun klere-fos--mu-metadata-bookcase-message-get (metadata)
  ""

  (alist-get :message (alist-get :type metadata)))

(defun klere-fos--mu-metadata-bookcase-position-get (metadata)
  ""

  (alist-get :position (alist-get :type metadata)))



(provide 'klere-fos--matrix-unit-metadata)

;;; klere-fos--matrix-unit-metadata.el ends here
