;;; klere-fos--matrices.el --- Klere Fòs: Matrices                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Klere-F-s
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0
;; Keywords: game, rpg

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(require 'klere-fos--utils)
;; if-let
(require 'subr-x)

(defun klere-fos--matrix-from-string (matrixstr)
  ""

  (let* ((split (split-string matrixstr "\n"))
         (first          (length (car split))))
    (apply #'vector
           (mapcar (lambda (elem)
                     (if (= (length elem) first)
                         (string-to-vector elem)
                       (error (concat "`klere-fos--matrix-from-string' requires "
                                      "the same length for each row.")))) split))))

(defun klere-fos--matrix-tile-type (tile)
  ""

  (if (consp tile) (car tile) tile))

(defun klere-fos--matrix-get (matrix pos)
  ""

  (if-let ((y                    (cdr pos))
           (long-enough (length> matrix y)))
      (if-let ((row          (elt matrix y))
               (x                 (car pos))
               (long-enough (length> row x)))
          (elt row x)
        (error "X coordinate is not within the bounds of the matrix."))
    (error "Y coordinate is not within the bounds of the matrix.")))



(defun klere-fos--matrix-get-coordinate-by-direction (directions matrix pos)
  ""

  (let ((        up-p                                   (member 'up   directions))
        (      left-p                                   (member 'left directions))
        (   up-down-p (or-eq (lambda (dir) (member dir directions)) 'up    'down))
        (left-right-p (or-eq (lambda (dir) (member dir directions)) 'right 'left)))
    (if-let* ((y          (funcall (if    up-down-p (if   up-p #'1- #'1+) #'identity)
                                   (cdr pos)))
              (x          (funcall (if left-right-p (if left-p #'1- #'1+) #'identity)
                                   (car pos)))
              (feasible-p (and
                            (funcall (if   up-p #'> #'<) y (if   up-p -1 (length matrix)))
                            (funcall (if left-p #'> #'<) x (if left-p -1 (length (elt matrix 0)))))))
        (cons x y)
      pos)))

(defun klere-fos--matrix-get-above-coordinate (matrix pos)
  "Get the coord. of MATRIX which is directly above POS. If outside, return nil.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 8 ('(1 . 2)), return '(1 . 1) and, for the
coordinates of tile 2 ('(1 . 0)), return '(1 . 0)."

  (klere-fos--matrix-get-coordinate-by-direction '(up) matrix pos))

(defun klere-fos--matrix-get-next-coordinate (matrix pos)
  "Get the coord. of MATRIX which is to the right of POS. If outside, return nil.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 8 ('(1 . 2)), return '(2 . 2) and, for the
coordinates of tile 3 ('(2 . 0)), return '(2 . 0)."

  (klere-fos--matrix-get-coordinate-by-direction '(right) matrix pos))

(defun klere-fos--matrix-get-prev-coordinate (matrix pos)
  "Get the coord. of MATRIX which is to the left of POS. If outside, return nil.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 8 ('(1 . 2)), return '(0 . 2) and, for the
coordinates of tile 1 ('(0 . 0)), return '(0 . 0)."

  (klere-fos--matrix-get-coordinate-by-direction '(left) matrix pos))

(defun klere-fos--matrix-get-below-coordinate (matrix pos)
  "Get the coord. of MATRIX which is directly below POS. If outside, return nil.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 2 ('(1 . 0)), return '(1 . 1) and, for the
coordinates of tile 8 ('(1 . 2)), return '(1 . 2)."

  (klere-fos--matrix-get-coordinate-by-direction '(down) matrix pos))

(defun klere-fos--matrix-get-next-above-coordinate (matrix pos)
  "Get the coord. of MATRIX which is diaginal, upward and rightward, of POS.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 8 ('(1 . 2)), return '(2 . 1) and, for the
coordinates of tile 9 ('(2 . 2)), return '(2 . 2)."

  (klere-fos--matrix-get-coordinate-by-direction '(up right) matrix pos))

(defun klere-fos--matrix-get-next-below-coordinate (matrix pos)
  "Get the coord. of MATRIX which is diaginal, downward and rightward, of POS.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 5 ('(1 . 1)), return '(2 . 2) and, for the
coordinates of tile 3 ('(0 . 2)), return '(0 . 2)."

  (klere-fos--matrix-get-coordinate-by-direction '(down right) matrix pos))

(defun klere-fos--matrix-get-prev-above-coordinate (matrix pos)
  "Get the coord. of MATRIX which is diaginal, upward and leftward, of POS.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 8 ('(1 . 2)), return '(0 . 1) and, for the
coordinates of tile 1 ('(0 . 0)), return '(0 . 0)."

  (klere-fos--matrix-get-coordinate-by-direction '(up left) matrix pos))

(defun klere-fos--matrix-get-prev-below-coordinate (matrix pos)
  "Get the coord. of MATRIX which is diaginal, downward and leftward, of POS.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 5 ('(1 . 1)), return '(0 . 2) and, for the
coordinates of tile 9 ('(2 . 2)), return '(2 . 2)."

  (klere-fos--matrix-get-coordinate-by-direction '(down left) matrix pos))



(defun klere-fos--matrix-get-tile-by-direction (directions matrix pos)
  ""

  (if-let* ((new-pos    (klere-fos--matrix-get-coordinate-by-direction directions
                                                                       matrix
                                                                       pos))
            (feasible-p (not (equal pos new-pos))))
      (klere-fos--matrix-get matrix new-pos)
    nil))

(defun klere-fos--matrix-get-above-tile (matrix pos)
  "Get the tile of MATRIX which is directly above POS. If outside, return nil.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 8, return 5 and, for the coordinates of tile 2,
return 'nil'."

  (klere-fos--matrix-get-tile-by-direction '(up) matrix pos))

(defun klere-fos--matrix-get-next-tile (matrix pos)
  "Get the tile of MATRIX which is to the right of POS. If outside, return nil.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 8, return 9 and, for the coordinates of tile 3,
return 'nil'."

  (klere-fos--matrix-get-tile-by-direction '(right) matrix pos))

(defun klere-fos--matrix-get-prev-tile (matrix pos)
  "Get the tile of MATRIX which is to the left of POS. If outside, return nil.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 8, return 7 and, for the coordinates of tile 1,
return 'nil'."

  (klere-fos--matrix-get-tile-by-direction '(left) matrix pos))

(defun klere-fos--matrix-get-below-tile (matrix pos)
  "Get the tile of MATRIX which is directly below POS. If outside, return nil.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 2, return 5 and, for the coordinates of tile 8,
return 'nil'."

  (klere-fos--matrix-get-tile-by-direction '(down) matrix pos))

(defun klere-fos--matrix-get-next-above-tile (matrix pos)
  "Get the tile of MATRIX which is diaginal, upward and rightward, of POS.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 8, return 6 and, for the coordinates of tile 9,
return 'nil'."

  (klere-fos--matrix-get-tile-by-direction '(up right) matrix pos))

(defun klere-fos--matrix-get-next-below-tile (matrix pos)
  "Get the tile of MATRIX which is diaginal, downward and rightward, of POS.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 5, return 9 and, for the coordinates of tile 3,
return 'nil'."

  (klere-fos--matrix-get-tile-by-direction '(down right) matrix pos))

(defun klere-fos--matrix-get-prev-above-tile (matrix pos)
  "Get the tile of MATRIX which is diaginal, upward and leftward, of POS.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 8, return 4 and, for the coordinates of tile 1,
return 'nil'."

  (klere-fos--matrix-get-tile-by-direction '(up left) matrix pos))

(defun klere-fos--matrix-get-prev-below-tile (matrix pos)
  "Get the tile of MATRIX which is diaginal, downward and leftward, of POS.

In a MATRIX such as:

1 2 3
4 5 6
7 8 9

for the coordinates of tile 5, return 7 and, for the coordinates of tile 9,
return 'nil'."

  (klere-fos--matrix-get-tile-by-direction '(down left) matrix pos))



(defun klere-fos--matrix-cycle-with-index (matrix funct &optional use-type)
  ""

  (let ((y 0))
    (mapcar
      (lambda (x-axis)
        (let ((x 0))
          (mapcar
            (lambda (tile)
              (when (or (not use-type)
                        (member (klere-fos--matrix-tile-type tile) use-type))
                (funcall funct tile x y))

              (setq x (1+ x)))
            x-axis))

        (setq y (1+ y)))
      matrix)))



(provide 'klere-fos--matrices)

;;; klere-fos--matrices.el ends here
