;;; klere-fos.el --- Klere Fòs                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Klere-F-s
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0
;; Keywords: game, rpg

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(require 'klere-fos--constants)
(require 'klere-fos--init-game)
;; if-let
(require 'subr-x)
;; toggle-truncate-lines
(require 'simple)

(defface klere-fos---start-splash-outer-rows
  '((t :foreground "#2A6893"))
  "Face for splash outer row decorations."
  :group 'klere-fos--start-mode)

(defface klere-fos---start-splash-middle-row
  '((t :foreground "#4D9BD2"))
  "Face for splash middle row decorations."
  :group 'klere-fos--start-mode)

(defface klere-fos---start-splash-inner-row
  '((t :foreground "#5dbcff"))
  "Face for splash inner row decorations."
  :group 'klere-fos--start-mode)

(defface klere-fos---start-splash-first-inline-reflection
  '((t :foreground "#2A6893" :weight bold))
  "Face for splash first inline reflection."
  :group 'klere-fos--start-mode)

(defface klere-fos---start-splash-second-inline-reflection
  '((t :foreground "#4D9BD2" :weight bold))
  "Face for splash second inline reflection."
  :group 'klere-fos--start-mode)

(defface klere-fos---start-splash-third-inline-reflection
  '((t :foreground "#1d7e34" :weight bold))
  "Face for splash third inline reflection."
  :group 'klere-fos--start-mode)

(defface klere-fos---start-splash-fourth-inline-reflection
  '((t :foreground "#725431" :weight bold))
  "Face for splash fourth inline reflection."
  :group 'klere-fos--start-mode)

(defcustom klere-fos---start-splash
  ;; (concat "   ▄█   ▄█▄  ▄█          ▄████████    ▄████████    ▄████████                    \n"
  ;;         "  ███ ▄███▀ ███         ███    ███   ███    ███   ███    ███                    \n"
  ;;         "  ███▐██▀   ███         ███    █▀    ███    ███   ███    █▀                     \n"
  ;;         " ▄█████▀    ███        ▄███▄▄▄      ▄███▄▄▄▄██▀  ▄███▄▄▄                        \n"
  ;;         "▀▀█████▄    ███       ▀▀███▀▀▀     ▀▀███▀▀▀▀▀   ▀▀███▀▀▀                        \n"
  ;;         "  ███▐██▄   ███         ███    █▄  ▀███████████   ███    █▄                     \n"
  ;;         "  ███ ▀███▄ ███▌    ▄   ███    ███   ███    ███   ███    ███                    \n"
  ;;         "  ███   ▀█▀ █████▄▄██   ██████████   ███    ███   ██████████                    \n"
  ;;         "  ▀         ▀                        ███    ███           ▄▄▄▄                  \n"
  ;;         "                                                         ▀▀▀█████▄              \n"
  ;;         "                                                                ▀▀█             \n"
  ;;         "                                               ▄████████  ▄██████▄     ▄████████\n"
  ;;         "                                              ███    ███ ███    ███   ███    ███\n"
  ;;         "                                              ███    █▀  ███    ███   ███    █▀ \n"
  ;;         "                                             ▄███▄▄▄     ███    ███   ███       \n"
  ;;         "                                            ▀▀███▀▀▀     ███    ███ ▀███████████\n"
  ;;         "                                              ███        ███    ███          ███\n"
  ;;         "                                              ███        ███    ███    ▄█    ███\n"
  ;;         "                                              ███         ▀██████▀   ▄████████▀ \n"
  ;;         "                                                                                \n"
  ;;         "                               Press RET to start                               ")
  (concat "                             " (propertize "- -- --- ---- --------- ---- --- -- -"
                                                      'face 'klere-fos---start-splash-outer-rows)
                                          "                                   \n"
          "                  " (propertize "-- == =============================================== == --"
                                           'face 'klere-fos---start-splash-outer-rows)
                               "                        \n"
          "       " (propertize "- -- === =============================================================== === -- -"
                                'face 'klere-fos---start-splash-middle-row)
                    "             \n"
          (propertize "- = == === =============================================================================== === == = -\n"
                      'face 'klere-fos---start-splash-inner-row)
          "                                                                        ____                         \n"
          "             __      __                                       _______   \\___\\                        \n"
          "             \\ \\    / / __       ______   ______    ______    \\_  __ \\    ___      ___               \n"
          "             | |   / /  \\ \\      \\  __ \\  \\  __ \\   \\  __ \\    | |  \\_\\  / _ \\    / _ \\              \n"
          "             |" (propertize "|"
                                       'face 'klere-fos---start-splash-first-inline-reflection)
                           "|  /"
                           (propertize "/"
                                       'face 'klere-fos---start-splash-first-inline-reflection)
                           "/   |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-first-inline-reflection)
                           "|      |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-first-inline-reflection)
                           "|  \\_\\ |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-first-inline-reflection)
                           "|  \\"
                           (propertize "\\"
                                       'face 'klere-fos---start-splash-first-inline-reflection)
                           "\\  |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-first-inline-reflection)
                           "|  \\_\\   |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-first-inline-reflection)
                           "|      /"
                           (propertize "/"
                                       'face 'klere-fos---start-splash-first-inline-reflection)
                           "/ \\"
                           (propertize "\\"
                                       'face 'klere-fos---start-splash-first-inline-reflection)
                           "\\  /"
                           (propertize "/"
                                       'face 'klere-fos---start-splash-first-inline-reflection)
                           "/ \\_\\             \n"
          "             |" (propertize "|"
                                       'face 'klere-fos---start-splash-second-inline-reflection)
                           "| /"
                           (propertize "/"
                                       'face 'klere-fos---start-splash-second-inline-reflection)
                           "/    |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-second-inline-reflection)
                           "|      |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-second-inline-reflection)
                           "|__    |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-second-inline-reflection)
                           "|   )"
                           (propertize ")"
                                       'face 'klere-fos---start-splash-second-inline-reflection)
                           ") |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-second-inline-reflection)
                           "|__      |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-second-inline-reflection)
                           "|     |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-second-inline-reflection)
                           "|   |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-second-inline-reflection)
                           "| \\"
                           (propertize "\\"
                                       'face 'klere-fos---start-splash-second-inline-reflection)
                           "\\__               \n"
          "             | |/ /     | |      |  __|   | |__/ /  |  __|     | |___  | |   | |  \\__ \\              \n"
          "             |   <      | |      | |      |    _/   | |        |  ___| | |   | |     \\ \\             \n"
          "             | |\\ \\     |" (propertize "|"
                                                    'face 'klere-fos---start-splash-third-inline-reflection)
                                        "|   __ |"
                                        (propertize "|"
                                                    'face 'klere-fos---start-splash-third-inline-reflection)
                                        "|   __ |"
                                        (propertize "|"
                                                    'face 'klere-fos---start-splash-third-inline-reflection)
                                        "|\\"
                                        (propertize "\\"
                                                    'face 'klere-fos---start-splash-third-inline-reflection)
                                        "\\    |"
                                        (propertize "|"
                                                    'face 'klere-fos---start-splash-third-inline-reflection)
                                        "|   __   |"
                                        (propertize "|"
                                                    'face 'klere-fos---start-splash-third-inline-reflection)
                                        "|     |"
                                        (propertize "|"
                                                    'face 'klere-fos---start-splash-third-inline-reflection)
                                        "|   |"
                                        (propertize "|"
                                                    'face 'klere-fos---start-splash-third-inline-reflection)
                                        "| __   )"
                                        (propertize ")"
                                                    'face 'klere-fos---start-splash-third-inline-reflection)
                                        ")            \n"
          "             |" (propertize "|"
                                       'face 'klere-fos---start-splash-third-inline-reflection)
                           "| \\"
                           (propertize "\\"
                                       'face 'klere-fos---start-splash-third-inline-reflection)
                           "\\    |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-fourth-inline-reflection)
                           "|__/ / |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-fourth-inline-reflection)
                           "|__/ / |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-fourth-inline-reflection)
                           "| \\"
                           (propertize "\\"
                                       'face 'klere-fos---start-splash-fourth-inline-reflection)
                           "\\__ |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-fourth-inline-reflection)
                           "|__/ /   |"
                           (propertize "|"
                                       'face 'klere-fos---start-splash-fourth-inline-reflection)
                           "|      \\"
                           (propertize "\\"
                                       'face 'klere-fos---start-splash-fourth-inline-reflection)
                           "\\_/"
                           (propertize "/"
                                       'face 'klere-fos---start-splash-fourth-inline-reflection)
                           "/  \\"
                           (propertize "\\"
                                       'face 'klere-fos---start-splash-fourth-inline-reflection)
                           "\\_/"
                           (propertize "/"
                                       'face 'klere-fos---start-splash-fourth-inline-reflection)
                           "/             \n"
          "             |" (propertize "|"
                                       'face 'klere-fos---start-splash-fourth-inline-reflection)
                           "|  \\"
                           (propertize "\\"
                                       'face 'klere-fos---start-splash-fourth-inline-reflection)
                           "\\__ /_____/  /_____/  |_|  \\__/ /_____/    /"
                           (propertize "/"
                                       'face 'klere-fos---start-splash-fourth-inline-reflection)
                           "/       \\___/    \\___/              \n"
          "             |_|   \\__/                                       /_/                                    \n"
          "                                                                                                     \n"
          "                                          Press RET to start                                         \n"
          "                                                                                                     \n"
          (propertize "- = == === =============================================================================== === == = -\n"
                      'face 'klere-fos---start-splash-inner-row)
          "       " (propertize "- -- === =============================================================== === -- -"
                                'face 'klere-fos---start-splash-middle-row)
                    "             \n"
          "                  " (propertize "-- == =============================================== == --"
                                           'face 'klere-fos---start-splash-outer-rows)
                               "                        \n"
          "                             " (propertize "- -- --- ---- --------- ---- --- -- -"
                                                      'face 'klere-fos---start-splash-outer-rows)
                                          "                                   ")
  "The text to display upon starting the game Klere.

It should, ideally, be 80x21 (if you split the string by the newlines) to
accomidate smaller screens.

This variable can either be a string – and it will be processed to split it by
the newlines in the string – or a list of strings with no newlines in any of the
strings.

Each string, or interval between newlines, should be equal in length."
  :type  '(choice string (list string))
  :group 'klere-fos--start)



(defun klere-fos-start-menu-begin ()
  ""
  (interactive)

  (klere-fos--init-game))

(defun klere-fos-start-menu-load ()
  ""
  (interactive)

  (if-let ((data (klere-fos--init-game-load)))
      (funcall (alist-get :save-place data))
    (message "No save files currently, yet, exist.")))

(defun klere-fos-start-menu-quit ()
  ""
  (interactive)

  (with-current-buffer klere-fos----buffer-name
    (let (kill-buffer-hook kill-buffer-query-functions)
      (kill-buffer))))

(defun klere-fos-start-menu-hint ()
  ""
  (interactive)

  (message (concat "Hint: press the button, in parenthases, next "
                   "to the menu item you'd like to select.")))

(defun klere-fos-start-menu-disabled ()
  ""
  (interactive)

  (message "That menu item's disabled!"))

(defface klere-fos---start-menu-disabled
  '((t :foreground "gray"))
  "Face for disabled menu options."
  :group 'klere-fos--start-menu-mode)

(defface klere-fos---start-menu-key
  '((t :weight bold))
  "Face for disabled menu options."
  :group 'klere-fos--start-menu-mode)

(defvar klere-fos--start-menu-mode-map (let ((mode-map (make-sparse-keymap)))
                                         ;; (define-key mode-map (kbd "R") #'klere-fos-start-hint)
                                         (define-key mode-map (kbd "?")   #'klere-fos-start-menu-hint)
                                         (define-key mode-map (kbd "RET") #'klere-fos-start-menu-hint)

                                         (define-key mode-map (kbd "n")   #'klere-fos-start-menu-begin)
                                         (define-key mode-map (kbd "l")   #'klere-fos-start-menu-load)
                                         (define-key mode-map (kbd "q")   #'klere-fos-start-menu-quit)

                                         mode-map)
  "Keymap for `klere-fos--start-menu-mode-mode'.")

(define-derived-mode klere-fos--start-menu-mode fundamental-mode "KlereFòs:Start-Menu"
  "Major mode for the Start menu in Klere Fòs."

  (read-only-mode 0)

  (let ((win-width  (1- (window-width)))
        (win-height (1- (window-height)))
        (menu       (list "┏━━━━━━━━━━━━━━━━━━━┓"
                          "┃                   ┃"
                          "┃                   ┃"
                          (concat "┃   New  Game "
                                  (propertize "(n)" 'face 'klere-fos---start-menu-key)
                                  "   ┃")
                          "┃                   ┃"
                          (concat "┃   "
                                  (if (klere-fos--init-game-get-saves)
                                      (concat "Load Game "
                                              (propertize "(l)"
                                                          'face
                                                          'klere-fos---start-menu-key))
                                    (propertize (concat "Load Game "
                                                        (propertize "(l)"
                                                                    'face
                                                                    'klere-fos---start-menu-key))
                                                'face 'klere-fos---start-menu-disabled))
                                  "   ┃")
                          "┃                   ┃"
                          (concat "┃   Quit Game "
                                  (propertize "(q)" 'face 'klere-fos---start-menu-key)
                                  "   ┃")
                          "┃                   ┃"
                          "┃                   ┃"
                          "┗━━━━━━━━━━━━━━━━━━━┛")))
    (dotimes (index (length menu))
      (let ((line (elt menu index)))
        (goto-line      (+ (/ (- win-height (length menu)) 2) index))
        (move-to-column    (/ (- win-width  (length line)) 2))

        (delete-char (length line))

        (insert line)))

    (read-only-mode 1)))



(defun klere-fos-start-hint ()
  ""
  (interactive)

  (message "Hint: press Enter!"))

(defun klere-fos-start-get-menu ()
  ""
  (interactive)

  (klere-fos--start-menu-mode))

(defvar klere-fos--start-mode-map (let ((mode-map (make-sparse-keymap)))
                                    (define-key mode-map (kbd "R")   #'klere-fos-start-hint)
                                    (define-key mode-map (kbd "r")   #'klere-fos-start-hint)

                                    (define-key mode-map (kbd "E")   #'klere-fos-start-hint)
                                    (define-key mode-map (kbd "e")   #'klere-fos-start-hint)

                                    (define-key mode-map (kbd "T")   #'klere-fos-start-hint)
                                    (define-key mode-map (kbd "t")   #'klere-fos-start-hint)

                                    (define-key mode-map (kbd "RET") #'klere-fos-start-get-menu)

                                    (define-key mode-map (kbd "q")   #'klere-fos-start-menu-quit)

                                    mode-map)
  "Keymap for `klere-fos--start-mode-mode'.")

(define-derived-mode klere-fos--start-mode fundamental-mode "KlereFòs:Start"
  "Major mode for the Start splash screen in Klere Fòs."

  (toggle-truncate-lines t)
  (read-only-mode        0)
  (erase-buffer)

  (let* ((win-width  (1- (window-width)))
         (win-height (1- (window-height)))
         (str        (let* ((s   (if (stringp klere-fos---start-splash)
                                     (split-string klere-fos---start-splash "\n")
                                   klere-fos---start-splash))
                            (s-w (length (car s)))
                            (s-h (length s))
                            (d-w (/ (- s-w win-width)  2))
                            (d-h (/ (- s-h win-height) 2)))
                       (mapcar (lambda (elem)
                                 (if (not (natnump d-w))
                                     elem
                                   (substring elem d-w (+ d-w win-width))))
                               (if (not (natnump d-h))
                                   s
                                 (butlast (nthcdr d-h s)
                                          (- s-h (+ d-h win-height)))))))
         (str-width  (length (car str)))
         (str-height (length str)))
    (insert (make-string (let ((n (/ (- win-height str-height) 2)))
                           (if (> n -1) n 0)) ?\n))

    (mapcar (lambda (elem)
              (insert (make-string (let ((n (/ (- win-width str-width) 2)))
                                     (if (> n -1) n 0)) ? )
                      elem
                      "\n")) str)

    (backward-delete-char 1)

    (goto-char (point-min))

    (read-only-mode 1)))



(defun klere-fos ()
  ""
  (interactive)

  (if-let ((existing (get-buffer klere-fos----buffer-name)))
      (switch-to-buffer existing)
    (switch-to-buffer klere-fos----buffer-name)

    (klere-fos--start-mode)))



(provide 'klere-fos)

;;; klere-fos.el ends here
