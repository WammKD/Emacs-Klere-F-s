;;; klere-fos--init-game.el --- Klere Fòs: Init Game                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Klere-F-s
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0
;; Keywords: game, rpg

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(require 'klere-fos--utils)
(require 'klere-fos--game-data)
(require 'klere-fos--dialog-exposition)
(require 'klere-fos--level1)
;; if-let
(require 'subr-x)

(defun klere-fos--init-game ()
  ""

  (klere-fos--dia-exp-render
    "...ah; hello, there. ▼"
    'witch-or-elf
    (klere-fos--with-curr-buff-lambda _buff
      (klere-fos--dia-exp-render
        (concat "Don't be alarmed. You're just..."
                "sleeping. You'll wake up, soon. ▼")
        'witch-or-elf
        (klere-fos--with-curr-buff-lambda _buff2
          (klere-fos--dia-exp-render
            "Tell me, can you remember your name, in this state? ▼"
            'witch-or-elf
            (let ((f (lambda (closure)
                       (klere-fos--with-curr-buff-lambda _buff3
                         (let ((name (read-string "What is your name? ")))
                           (if (string-empty-p name)
                               (klere-fos--dia-exp-render
                                 "Come, now: you can do this; remember. ▼"
                                 'witch-or-elf
                                 (funcall closure closure))
                             (klere-fos--dia-exp-render
                               (concat (mapconcat #'char-to-string
                                                  (string-to-list name)
                                                  " ")
                                       ": that's a good name, "
                                       name
                                       ". ▼")
                               'witch-or-elf
                               (klere-fos--with-curr-buff-lambda _buff4
                                 (klere-fos--game-data-new-game name)

                                 (klere-fos--dia-exp-render
                                   (concat "Well, " name ", it's time to wake "
                                           "up; you're needed, " name ". ▼")
                                   'witch-or-elf
                                   (klere-fos--with-curr-buff-lambda _b
                                     (klere-fos--level1)))))))))))
              (funcall f f))))))))

(defun klere-fos--init-game-get-saves ()
  ""

  (when-let ((exists-p (file-directory-p klere-fos----game-data-path)))
    (directory-files klere-fos----game-data-path
                     nil
                     directory-files-no-dot-files-regexp)))

(defun klere-fos--init-game-load ()
  ""

  (when-let* ((save-files  (mapcar
                             (apply-partially #'concat klere-fos----game-data-path)
                             (klere-fos--init-game-get-saves)))
              (completions (mapcar
                             (lambda (path)
                               (let ((data (klere-fos--game-data-fetch path)))
                                 (cons (concat (alist-get :user-name data)
                                               " – "
                                               (format-time-string
                                                 "%B %d, %Y"
                                                 (alist-get :last-play-start data)))
                                       data)))
                             save-files)))
    (klere-fos--game-data-load (alist-get (ido-completing-read "Load which save? "
                                                               completions)
                                          completions))))



(provide 'klere-fos--init-game)

;;; klere-fos--init-game.el ends here
